import { PaymentMethod } from './PaymentMethod';

export interface PaymentProfile {
  userId: string;
  defaultPaymentMethod: PaymentMethod | null;
  paymentMethods: PaymentMethod[] | null;
}
