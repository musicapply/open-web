import { LibraryArtistRecord } from './LibraryArtistRecord';

export interface LibraryArtistsState {
  libraryArtists: LibraryArtistRecord[] | null;
}
