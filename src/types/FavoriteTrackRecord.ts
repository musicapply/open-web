import { Track } from './Track';

export interface FavoriteTrackRecord {
  id: string;
  creationDate: string;
  track: Track;
}
