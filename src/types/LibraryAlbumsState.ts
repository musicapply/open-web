import { LibraryAlbumRecord } from './LibraryAlbumRecord';

export interface LibraryAlbumsState {
  libraryAlbums: LibraryAlbumRecord[] | null;
}
