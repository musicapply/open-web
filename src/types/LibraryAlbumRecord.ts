import { Album } from './Album';

export interface LibraryAlbumRecord {
  id: string;
  creationDate: string;
  album: Album;
}
