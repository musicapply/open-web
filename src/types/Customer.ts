export interface Customer {
  userId: string;
  subscriptionId: string;
  start: string;
  nextPayment: string;
  productId: string;
  status: string;
}
