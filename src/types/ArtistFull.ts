import { AlbumShort } from './AlbumShort';

export interface ArtistFull {
  artistId: string;
  imageUrl: string;
  name: string;
  bio: string;
  albums: AlbumShort[];
}
