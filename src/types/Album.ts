import { ArtistShort } from './ArtistShort';
import { Track } from './Track';

export interface Album {
  albumId: string;
  artist: ArtistShort;
  name: string;
  description: string;
  releaseDate: string;
  imageUrl: string;
  tracks: Track[];
}
