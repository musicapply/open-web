import { Playlist } from './Playlist';

export interface PlaylistsState {
  playlists: Playlist[] | null;
}
