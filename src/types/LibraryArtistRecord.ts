import { ArtistFull } from './ArtistFull';

export interface LibraryArtistRecord {
  id: string;
  creationDate: string;
  artist: ArtistFull;
}
