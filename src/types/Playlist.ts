import { Track } from './Track';
import { PlaylistTrackRecord } from './PlaylistTrackRecord';

export interface Playlist {
  playlistId: string;
  userId: string;
  name: string;
  creationDate: string;
  tracks: PlaylistTrackRecord[];
}
