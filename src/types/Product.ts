export interface Product {
  id: string;
  name: string;
  description: string;
  imageUrl: string | null | undefined;
  unitAmount: number;
  currency: string;
}
