import { Transaction } from './Transaction';

export interface TransactionsState {
  transactions: Transaction[] | null;
}
