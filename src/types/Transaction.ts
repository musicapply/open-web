import { Product } from './Product';

export interface Transaction {
  userId: string;
  amountPaid: number;
  currency: string;
  paymentDate: string;
  product: Product;
  productId: string;
  paymentMethodId: string;
}
