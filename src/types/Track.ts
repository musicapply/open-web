import { AlbumShort } from './AlbumShort';
import { ArtistShort } from './ArtistShort';

export interface Track {
  trackId: string;
  name: string;
  fileUrl: string;
  album: AlbumShort;
  artist: ArtistShort;
}
