export interface AlbumShort {
  albumId: string;
  artistId: string;
  name: string;
  description: string;
  releaseDate: string;
  imageUrl: string;
}
