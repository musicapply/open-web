export interface AddTrackToPlaylistRequest {
  playlistId: string;
  trackId: string;
}
