import { Track } from './Track';

export interface CurrentPlayerState {
  track: Track | null;
  loop: boolean;
}
