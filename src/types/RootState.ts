// [IMPORT NEW CONTAINERSTATE ABOVE] < Needed for generating containers seamlessly
import { ThemeState } from 'styles/theme/slice/types';
import { UserState } from './UserState';
import { PaymentProfileState } from './PaymentProfileState';
import { TransactionsState } from './TransactionsState';
import { FavoriteTracksState } from './FavoriteTracksState';
import { ContextMenuState } from './ContextMenuState';
import { LibraryAlbumsState } from './LibraryAlbumsState';
import { LibraryArtistsState } from './LibraryArtistsState';
import { PlayerState } from './PlayerState';
import { PlaylistsState } from './PlaylistsState';
import { CustomerState } from './CustomerState';

/*
  Because the redux-injectors injects your reducers asynchronously somewhere in your code
  You have to declare them here manually
*/
export interface RootState {
  theme?: ThemeState;
  user?: UserState;
  paymentProfile?: PaymentProfileState;
  transactions?: TransactionsState;
  favorite?: FavoriteTracksState;
  contextMenu?: ContextMenuState;
  libraryAlbums?: LibraryAlbumsState;
  libraryArtists?: LibraryArtistsState;
  player?: PlayerState;
  playlists?: PlaylistsState;
  customer: CustomerState;
  // [INSERT NEW REDUCER KEY ABOVE] < Needed for generating containers seamlessly
}
