export interface RemoveTrackFromPlaylistRequest {
  playlistId: string;
  trackRecordId: string;
}
