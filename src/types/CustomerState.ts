import { Customer } from './Customer';

export interface CustomerState {
  customer: Customer | null;
}
