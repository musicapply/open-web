export interface PaymentMethod {
  paymentMethodId: string;
  cardNumber: string;
  cardBrand: string;
}
