import { CurrentPlayerState } from './CurrentPlayerState';

export interface PlayerState {
  playerState: CurrentPlayerState;
}
