import { Track } from './Track';

export interface PlaylistTrackRecord {
  id: string;
  creationDate: string;
  track: Track;
}
