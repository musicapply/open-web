export interface ArtistShort {
  artistId: string;
  imageUrl: string;
  name: string;
  bio: string;
}
