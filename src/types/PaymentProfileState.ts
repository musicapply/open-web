import { PaymentProfile } from './PaymentProfile';

export interface PaymentProfileState {
  paymentProfile: PaymentProfile | null;
}
