export interface AddPaymentMethodRequest {
  cardNumber: string;
  expMonth: string;
  expYear: string;
  cvc: string;
}
