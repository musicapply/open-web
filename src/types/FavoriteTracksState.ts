import { FavoriteTrackRecord } from './FavoriteTrackRecord';

export interface FavoriteTracksState {
  favorite: FavoriteTrackRecord[] | null;
}
