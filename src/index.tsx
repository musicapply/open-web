import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import 'sanitize.css/sanitize.css';

import { App } from 'app';

import { HelmetProvider } from 'react-helmet-async';
import { ThemeProvider } from 'styles/theme/ThemeProvider';

import { configureAppStore } from 'store/configureStore';

import reportWebVitals from 'reportWebVitals';

import './locales/i18n';

import { ReactKeycloakProvider } from '@react-keycloak/web';
import keycloak from './utils/Keycloak';
import {
  saveAccessTokenToStorage,
  saveRefreshTokenToStorage,
} from './utils/tokenUtils';

const store = configureAppStore();
const MOUNT_NODE = document.getElementById('root') as HTMLElement;

const onTokenHandler = tokens => {
  saveAccessTokenToStorage(tokens.token);
  saveRefreshTokenToStorage(tokens.refreshToken);
};

ReactDOM.render(
  <ReactKeycloakProvider authClient={keycloak} onTokens={onTokenHandler}>
    <Provider store={store}>
      <ThemeProvider>
        <HelmetProvider>
          <React.StrictMode>
            <App />
          </React.StrictMode>
        </HelmetProvider>
      </ThemeProvider>
    </Provider>
  </ReactKeycloakProvider>,
  MOUNT_NODE,
);

if (module.hot) {
  module.hot.accept(['./locales/i18n'], () => {});
}

reportWebVitals();
