import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { BrowserRouter, Switch } from 'react-router-dom';

import { GlobalStyle } from 'styles/global-styles';
import { StyleConstants } from 'styles/StyleConstants';

import { PlayingBar } from './components/PlayingBar';
import { RootNavBar } from './components/RootNavBar';
import { TopBar } from './components/TopBar';

import { HomePage } from './pages/HomePage/Loadable';
import { ArtistsPage } from './pages/collection/Artists/Loadable';
import { NotFoundPage } from './components/NotFoundPage/Loadable';
import { FavoriteTracksPage } from './pages/collection/Tracks/Loadable';
import { UserPage } from './pages/User/Loadable';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components/macro';
import { useKeycloak } from '@react-keycloak/web';
import { PrivateRoute } from '../utils/PrivateRoute';
import { media } from '../styles/media';
import { MobileAppBar } from './components/MobileAppBar';
import { useContextMenuSlice } from './components/ContextMenu/slice';
import { useDispatch, useSelector } from 'react-redux';
import { AlbumPage } from './pages/Album/Loadable';
import { ArtistPage } from './pages/Artist/Loadable';
import { SearchPage } from './pages/Search/Loadable';
import { PlaylistsPage } from './pages/collection/Playlists/Loadable';
import { useFavoriteSlice } from './pages/collection/Tracks/slice';
import { createRef, useEffect, useRef, useState } from 'react';
import { AlbumsPage } from './pages/collection/Albums/Loadable';
import { useLibraryAlbumsSlice } from './pages/collection/Albums/slice';
import { useLibraryArtistSlice } from './pages/collection/Artists/slice';
import { selectPlayerState } from './components/PlayingBar/slice/selector';
import { savePlayerStateToLocalStorage } from './components/PlayingBar/slice/utils';
import { CurrentPlayerState } from '../types/CurrentPlayerState';
import { usePlaylistsSlice } from './pages/collection/Playlists/slice';
import { PlaylistPage } from './pages/Playlist/Loadable';
import Popup from 'reactjs-popup';
import { NewPlaylistCard } from './components/NewPlaylistCard/NewPlaylistCard';
import { LoadingComponent } from './components/LoadingComponent';

export function App() {
  const { i18n } = useTranslation();

  const playerState: CurrentPlayerState = useSelector(selectPlayerState);

  const { initialized, keycloak } = useKeycloak();
  const { actions: contextMenuActions } = useContextMenuSlice();
  const { actions: favoriteActions } = useFavoriteSlice();
  const { actions: libraryAlbumsActions } = useLibraryAlbumsSlice();
  const { actions: libraryArtistsActions } = useLibraryArtistSlice();
  const { actions: playlistsActions } = usePlaylistsSlice();
  const dispatch = useDispatch();

  const useEffectOnMount = (effect: React.EffectCallback) => {
    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(effect, []);
  };

  useEffect(() => {
    savePlayerStateToLocalStorage(playerState);
  });

  useEffectOnMount(() => {
    dispatch(playlistsActions.loadPlaylists());
    dispatch(favoriteActions.loadFavorite());
    dispatch(libraryAlbumsActions.loadAlbums());
    dispatch(libraryArtistsActions.loadArtists());
  });

  const [isOpenPlaylistDialog, setIsOpenPlaylistDialog] = useState(false);

  if (!initialized) {
    return <LoadingComponent />;
  }

  // @ts-ignore
  return (
    <BrowserRouter>
      <Helmet
        titleTemplate="MusicApply - %s"
        defaultTitle="MusicApply"
        htmlAttributes={{ lang: i18n.language }}
      >
        <meta name="description" content="Listen to what you like" />
      </Helmet>

      <TopContainer onClick={e => dispatch(contextMenuActions.setShow(false))}>
        <MainView>
          <RootNavBar
            openNewPlaylistDialog={() => setIsOpenPlaylistDialog(true)}
          />
          <ContentView>
            <TopBar />
            <ContentWrapper>
              {/*// @ts-ignore*/}
              <Popup
                open={isOpenPlaylistDialog}
                onClose={() => setIsOpenPlaylistDialog(false)}
                position="center center"
                modal
                nested
              >
                {/*// @ts-ignore*/}
                {close => <NewPlaylistCard closeCallback={close} />}
              </Popup>
              {/*// @ts-ignore*/}
              <Switch>
                <PrivateRoute
                  exact
                  roles={['user']}
                  path="/"
                  component={HomePage}
                />
                <PrivateRoute
                  exact
                  roles={['user']}
                  path="/profile"
                  component={UserPage}
                />
                <PrivateRoute
                  exact
                  roles={['user']}
                  path="/search/:searchArg"
                  component={SearchPage}
                />
                <PrivateRoute
                  exact
                  roles={['user']}
                  path="/album/:albumId"
                  component={AlbumPage}
                />
                <PrivateRoute
                  exact
                  roles={['user']}
                  path="/artist/:artistId"
                  component={ArtistPage}
                />
                <PrivateRoute
                  exact
                  roles={['user']}
                  path="/collection/albums"
                  component={AlbumsPage}
                />
                <PrivateRoute
                  exact
                  roles={['user']}
                  path="/collection/artists"
                  component={ArtistsPage}
                />
                <PrivateRoute
                  exact
                  roles={['user']}
                  path="/collection/playlists"
                  component={PlaylistsPage}
                />
                <PrivateRoute
                  exact
                  roles={['user']}
                  path="/playlist/:playlistId"
                  component={PlaylistPage}
                />
                <PrivateRoute
                  exact
                  roles={['user']}
                  path="/collection/tracks"
                  component={FavoriteTracksPage}
                />
                <PrivateRoute roles={['user']} component={NotFoundPage} />
              </Switch>
            </ContentWrapper>
          </ContentView>
        </MainView>
        <PlayingBar />
        <MobileAppBar />
      </TopContainer>
      <GlobalStyle />
    </BrowserRouter>
  );
}

const TopContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100vh;
`;

const MainView = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  height: calc(100% - 165px);

  ${media.small`
    height: calc(100% - 165px);
  `}

  ${media.medium`
    height: calc(100% - ${StyleConstants.PLAYING_BAR_HEIGHT});
  `}

  ${media.large`
    height: calc(100% - ${StyleConstants.PLAYING_BAR_HEIGHT});
  `}

  ${media.xlarge`
    height: calc(100% - ${StyleConstants.PLAYING_BAR_HEIGHT});
  `}
`;

const ContentView = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  background: ${p => p.theme.primary};
`;

const ContentWrapper = styled.div`
  display: flex;
  width: calc(100% - 15px);
  height: 100%;
  overflow-y: auto;
`;
