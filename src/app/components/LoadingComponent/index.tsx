import * as React from 'react';
import styled, { useTheme } from 'styled-components/macro';
import { ImageViewer } from '../ImageViewer/ImageViewer';

export function LoadingComponent() {
  return (
    <Wrapper>
      <ImageViewer
        width={200}
        height={200}
        imageUrl={require('./assets/logo512.png')}
      />
    </Wrapper>
  );
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100vh;
  justify-content: center;
  align-items: center;
  background: ${p => p.theme.background};
`;
