import * as React from 'react';
import styled, { useTheme } from 'styled-components/macro';
import { StyleConstants } from 'styles/StyleConstants';

import { NavLink, Link } from 'react-router-dom';

import { ReactComponent as HomeSvg } from './assets/Home.svg';
import { ReactComponent as HomeDarkSvg } from './assets/HomeDark.svg';
import { ReactComponent as BrowseSvg } from './assets/Browse.svg';
import { ReactComponent as BrowseDarkSvg } from './assets/BrowseDark.svg';
import { ReactComponent as LikedSvg } from './assets/Liked.svg';
import { ReactComponent as LikedDarkSvg } from './assets/LikedDark.svg';
import { rgba } from 'polished';
import { media } from 'styles/media';
import { useSelector } from 'react-redux';
import { selectPlaylists } from '../../pages/collection/Playlists/slice/selector';

export function RootNavBar(props) {
  const theme = useTheme();

  const playlists = useSelector(selectPlaylists);

  return (
    <Wrapper>
      <NavigationLinks>
        <MarginConstraint>
          <NavButton to="/" exact>
            {theme.type === 'dark' ? <HomeIconDark /> : <HomeIcon />}
            <span>Главная</span>
          </NavButton>
          <NavButton to="/profile" exact>
            {theme.type === 'dark' ? <HomeIconDark /> : <HomeIcon />}
            <span>Профиль</span>
          </NavButton>
          <NavButton to="/collection/tracks" exact>
            {theme.type === 'dark' ? <LikedIconDark /> : <LikedIcon />}
            <span>Избранное</span>
          </NavButton>
        </MarginConstraint>
      </NavigationLinks>
      <LibraryLinks>
        <LibraryHeader>Библиотека</LibraryHeader>
        <LibraryLink to="/collection/albums">Альбомы</LibraryLink>
        <LibraryLink to="/collection/artists">Исполнители</LibraryLink>
      </LibraryLinks>
      <Playlists>
        <PlaylistsHeader>Плейлисты</PlaylistsHeader>
        <NewPlaylistButton onClick={props.openNewPlaylistDialog}>
          НОВЫЙ ПЛЕЙЛИСТ
        </NewPlaylistButton>
        <PlaylistScroll>
          {playlists?.map(playlist => (
            <Playlist
              key={playlist.playlistId}
              to={`/playlist/${playlist.playlistId}`}
            >
              <span>{playlist.name}</span>
            </Playlist>
          ))}
        </PlaylistScroll>
      </Playlists>
    </Wrapper>
  );
}

const NewPlaylistButton = styled.button`
  border-radius: 10px;
  height: 46px;
  min-height: 46px;
  max-height: 46px;
  background: ${p => p.theme.btn};
  border: none;
  font-size: 1.25rem;
  font-weight: bolder;
  color: ${p => p.theme.text.primary};
`;

const MarginConstraint = styled.div`
  margin-top: 15px;
  margin-left: 15px;
  margin-right: 15px;
`;

const Wrapper = styled.div`
  display: none;
  width: ${StyleConstants.ROOT_NAV_BAR_WIDTH};
  min-width: ${StyleConstants.ROOT_NAV_BAR_WIDTH};
  flex-direction: column;
  background: ${p => p.theme.background};

  ${media.small`
    display: none
  `}

  ${media.medium`
    display: flex
  `}

  ${media.large`
    display: flex
  `}

  ${media.xlarge`
    display: flex
  `}
`;

const NavigationLinks = styled.div`
  display: flex;
  flex-direction: column;
`;

const Playlists = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 30px;
  margin-right: 15px;
  margin-top: 25px;
  height: 100%;
  overflow-y: hidden;
  overflow-x: hidden;
`;

const PlaylistScroll = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  overflow-y: auto;
  overflow-x: hidden;
`;

const PlaylistsHeader = styled.p`
  font-size: 0.95rem;
  text-transform: uppercase;
  margin-top: 0;
  margin-bottom: 0;
`;

const Playlist = styled(Link)`
  margin-top: 0;
  margin-bottom: 0;
  text-decoration: none;
  color: inherit;
  font-weight: bolder;
  font-size: 1.25rem;

  span {
    display: block;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
`;

const LibraryLink = styled(Link)`
  margin-top: 0;
  margin-bottom: 0;
  text-decoration: none;
  color: inherit;
  font-weight: bolder;
  font-size: 1.25rem;
`;

const LibraryLinks = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 30px;
  margin-right: 15px;
  margin-top: 25px;
`;

const LibraryHeader = styled.p`
  font-size: 0.95rem;
  text-transform: uppercase;
  margin-top: 0;
  margin-bottom: 0;
`;

const HomeIcon = styled(HomeSvg)`
  width: ${StyleConstants.ROOT_NAV_BAR_BUTTON_ICON_WIDTH};
  height: ${StyleConstants.ROOT_NAV_BAR_BUTTON_ICON_HEIGHT};
`;

const HomeIconDark = styled(HomeDarkSvg)`
  width: ${StyleConstants.ROOT_NAV_BAR_BUTTON_ICON_WIDTH};
  height: ${StyleConstants.ROOT_NAV_BAR_BUTTON_ICON_HEIGHT};
`;

const BrowseIcon = styled(BrowseSvg)`
  width: ${StyleConstants.ROOT_NAV_BAR_BUTTON_ICON_WIDTH};
  height: ${StyleConstants.ROOT_NAV_BAR_BUTTON_ICON_HEIGHT};
`;

const BrowseIconDark = styled(BrowseDarkSvg)`
  width: ${StyleConstants.ROOT_NAV_BAR_BUTTON_ICON_WIDTH};
  height: ${StyleConstants.ROOT_NAV_BAR_BUTTON_ICON_HEIGHT};
`;

const LikedIcon = styled(LikedSvg)`
  width: ${StyleConstants.ROOT_NAV_BAR_BUTTON_ICON_WIDTH};
  height: ${StyleConstants.ROOT_NAV_BAR_BUTTON_ICON_HEIGHT};
`;

const LikedIconDark = styled(LikedDarkSvg)`
  width: ${StyleConstants.ROOT_NAV_BAR_BUTTON_ICON_WIDTH};
  height: ${StyleConstants.ROOT_NAV_BAR_BUTTON_ICON_HEIGHT};
`;

const NavButton = styled(NavLink)`
  background: transparent;
  color: ${p => p.theme.text.primary};
  border-radius: 10px;
  border: none;
  height: 35px;
  font-size: 1.45rem;
  display: flex;
  flex-direction: row;
  text-decoration: none;

  &.active {
    background: ${p => rgba(p.theme.green, 0.15)};
  }

  &:visited {
    color: inherit;
  }

  svg {
    align-self: center;
    margin-left: 20px;
  }

  span {
    align-self: center;
    margin-left: 5px;
    line-height: 0;
  }
`;
