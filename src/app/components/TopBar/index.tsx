import * as React from 'react';
import styled, { useTheme } from 'styled-components/macro';
import { StyleConstants } from 'styles/StyleConstants';
import Avatar from '../Avatar';
import { SearchBar } from '../SearchBar';
import { useSelector } from 'react-redux';
import { selectUser } from 'app/pages/User/slice/selectors';

import { ReactComponent as BackButtonDarkIconSvg } from './assets/BackButtonDarkIcon.svg';
import { ReactComponent as NextButtonDarkIconSvg } from './assets/NextButtonDarkIcon.svg';
import { ReactComponent as BackButtonLightIconSvg } from './assets/BackButtonLightIcon.svg';
import { ReactComponent as NextButtonLightIconSvg } from './assets/NextButtonLightIcon.svg';
import { useHistory } from 'react-router-dom';
import { media } from 'styles/media';

export function TopBar() {
  const user = useSelector(selectUser);
  const history = useHistory();
  const theme = useTheme();

  return (
    <Wrapper>
      <NavigationButtonWrapper>
        <BackButton onClick={() => history.goBack()}>
          {theme.type === 'dark' ? (
            <BackButtonDarkIcon />
          ) : (
            <BackButtonLightIcon />
          )}
        </BackButton>
        <NextButton onClick={() => history.goForward()}>
          {theme.type === 'dark' ? (
            <NextButtonDarkIcon />
          ) : (
            <NextButtonLightIcon />
          )}
        </NextButton>
      </NavigationButtonWrapper>
      <SearchBar />
      <Avatar
        imageUrl="https://sun9-41.userapi.com/s/v1/ig2/IUKDoPh4IatfaPl2fOIsGjfmSEMHbfw2aAcgNScTn4j_VXdVB70dhAM5AOOjXJai_YSGuyISg7_sz48X3IMGGyVI.jpg?size=1080x1029&quality=96&type=album"
        username=""
        size="sm"
      />
    </Wrapper>
  );
}

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  padding-left: 10px;
  padding-right: 10px;
  height: ${StyleConstants.TOP_BAR_HEIGHT};
  min-height: ${StyleConstants.TOP_BAR_HEIGHT};
  flex-direction: row;
  background: ${p => p.theme.primary};

  ${media.small`
    padding-left: 10px;
    padding-right: 10px;
  `}

  ${media.medium`
    padding-left: 54px;
    padding-right: 54px;
  `}

  ${media.large`
    padding-left: 54px;
    padding-right: 54px;
  `}

  ${media.xlarge`
    padding-left: 54px;
    padding-right: 54px;
  `}
`;

const NavigationButtonWrapper = styled.div`
  display: flex;
`;

const BackButton = styled.button`
  background: ${p => p.theme.background};
  height: 48px;
  min-width: 55px;
  border: none;
  border-top-left-radius: 6px;
  border-bottom-left-radius: 6px;
`;

const NextButton = styled.button`
  background: ${p => p.theme.background};
  height: 48px;
  min-width: 55px;
  border: none;
  border-top-right-radius: 6px;
  border-bottom-right-radius: 6px;
`;

const BackButtonLightIcon = styled(BackButtonLightIconSvg)`
  width: 35px;
  height: 35px;
`;

const NextButtonLightIcon = styled(NextButtonLightIconSvg)`
  width: 35px;
  height: 35px;
`;

const BackButtonDarkIcon = styled(BackButtonDarkIconSvg)`
  width: 35px;
  height: 35px;
`;

const NextButtonDarkIcon = styled(NextButtonDarkIconSvg)`
  width: 35px;
  height: 35px;
`;
