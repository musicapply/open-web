import { createSelector } from '@reduxjs/toolkit';
import { RootState } from 'types';
import { initialState } from './index';

const selectDomain = (state: RootState) => state.contextMenu || initialState;

export const selectShowContextMenu = createSelector(
  [selectDomain],
  contextMenu => contextMenu?.show,
);
