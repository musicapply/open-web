import { ContextMenuState } from 'types/ContextMenuState';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { PayloadAction } from '@reduxjs/toolkit';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';

export const initialState: ContextMenuState = {
  show: false,
};

export const slice = createSlice({
  name: 'contextMenu',
  initialState,
  reducers: {
    setShow(state, action: PayloadAction<boolean>) {
      state.show = action.payload;
    },
  },
});

export const { actions: contextMenuActions, reducer } = slice;

export const useContextMenuSlice = () => {
  useInjectReducer({
    key: slice.name,
    reducer: slice.reducer,
  });
  return {
    actions: slice.actions,
  };
};
