import * as React from 'react';
import styled from 'styled-components/macro';
import { rem } from 'polished';
import PropTypes from 'prop-types';

const Avatar = ({ imageUrl, username, size }) => (
  <Wrapper size={size}>
    <Image src={imageUrl} alt={username} />
  </Wrapper>
);

Avatar.defaultProps = {
  size: 'md',
};

Avatar.propTypes = {
  imageUrl: PropTypes.string.isRequired,
  size: PropTypes.oneOf(['sm', 'md', 'lg', 'xlg']),
  username: PropTypes.string.isRequired,
};

const sizeUnit = 16;

const createSize = size => rem(`${size * sizeUnit}px`);

const sizes = {
  sm: createSize(3),
  md: createSize(4),
  lg: createSize(5),
  xlg: createSize(10),
};

const size = size => {
  if (typeof sizes[size] !== 'undefined') {
    return sizes[size];
  } else {
    return sizes['md'];
  }
};

const Image = styled.img`
  display: block;
  border-radius: 50%;
  width: 100%;
  height: auto;
  user-select: none;
  pointer-events: none;
`;

const Wrapper = styled.div.attrs((props: { size: string }) => props)`
  pointer-events: none;
  user-select: none;
  box-sizing: border-box;
  border-radius: 50%;
  padding: ${rem('2px')};
  width: ${props => size(props.size)};
  min-width: ${props => size(props.size)};
  height: ${props => size(props.size)};
  min-height: ${props => size(props.size)};
`;

export default Avatar;
