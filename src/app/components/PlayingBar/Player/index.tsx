import * as React from 'react';
import styled, { useTheme } from 'styled-components/macro';
import { createRef, useEffect, useRef, useState } from 'react';
import { Track } from 'types/Track';
import { CurrentPlayerState } from 'types/CurrentPlayerState';

import { ReactComponent as PlayerStopDarkSvg } from './assets/PlayerStopDark.svg';
import { ReactComponent as PlayerStopLightSvg } from './assets/PlayerStopLight.svg';
import { ReactComponent as PlayerNextDarkSvg } from './assets/PlayerNextDark.svg';
import { ReactComponent as PlayerNextLightSvg } from './assets/PlayerNextLight.svg';
import { useKeycloak } from '@react-keycloak/web';

interface PlayerProps {
  state: CurrentPlayerState;
}

export function Player(props: PlayerProps) {
  const theme = useTheme();

  const { initialized, keycloak } = useKeycloak();

  const [trackProgress, setTrackProgress] = useState(0);
  const [isPlaying, setIsPlaying] = useState(false);

  const currentState: CurrentPlayerState = props.state;

  const audioRef = useRef(new Audio(currentState.track?.fileUrl));
  const intervalRef = useRef();
  const isReady = useRef(false);
  const { duration } = audioRef.current;

  useEffect(() => {
    if (isPlaying) {
      //@ts-ignore
      if (
        keycloak.tokenParsed.realm_access.roles.indexOf('premium_user') !== -1
      ) {
        audioRef.current.play();
        startTimer();
      }
    } else {
      clearInterval(intervalRef.current);
      audioRef.current.pause();
    }
  }, [isPlaying]);

  useEffect(() => {
    // Pause and clean up on unmount

    return () => {
      audioRef.current.pause();
      clearInterval(intervalRef.current);
    };
  }, []);

  useEffect(() => {
    audioRef.current.pause();

    audioRef.current = new Audio(currentState.track?.fileUrl);
    setTrackProgress(audioRef.current.currentTime);

    if (isReady.current) {
      //@ts-ignore
      if (
        keycloak.tokenParsed.realm_access.roles.indexOf('premium_user') !== -1
      ) {
        audioRef.current.play();
        setIsPlaying(true);
      }
      startTimer();
    } else {
      // Set the isReady ref as true for the next pass
      isReady.current = true;
    }
  }, [currentState.track]);

  const startTimer = () => {
    // Clear any timers already running
    clearInterval(intervalRef.current);

    // @ts-ignore
    intervalRef.current = setInterval(() => {
      if (audioRef.current.ended) {
        console.log('audio ended');
      } else {
        setTrackProgress(audioRef.current.currentTime);
      }
      // @ts-ignore
    }, [1000]);
  };

  const onScrub = value => {
    // Clear any timers already running
    clearInterval(intervalRef.current);
    audioRef.current.currentTime = value;
    setTrackProgress(audioRef.current.currentTime);
  };

  const onScrubEnd = () => {
    // If not already playing, start
    if (!isPlaying) {
      setIsPlaying(true);
    }
    startTimer();
  };

  const calculateCurrentPlayerTimeMinutes = num => {
    return Math.trunc(trackProgress / 60);
  };

  const calculateCurrentPlayerTimeSeconds = num => {
    const value =
      Math.trunc(trackProgress) - Math.trunc(trackProgress / 60) * 60;
    if (value < 10) {
      return '0' + value;
    }
    return value;
  };

  // @ts-ignore
  return (
    <Wrapper>
      <StateButtonsWrapper>
        {isPlaying ? (
          theme.type === 'dark' ? (
            <PlayerStopDark onClick={() => setIsPlaying(false)} />
          ) : (
            <PlayerStopLight onClick={() => setIsPlaying(false)} />
          )
        ) : theme.type === 'dark' ? (
          <PlayerNextDark onClick={() => setIsPlaying(true)} />
        ) : (
          <PlayerNextLight onClick={() => setIsPlaying(true)} />
        )}
      </StateButtonsWrapper>
      <PlayerStateWrapper>
        <span>
          {calculateCurrentPlayerTimeMinutes(trackProgress)}:
          {calculateCurrentPlayerTimeSeconds(trackProgress)}
        </span>
        <PlayState
          type="range"
          value={trackProgress}
          step="1"
          min="0"
          max={duration ? duration : `${duration}`}
          className="progress"
          onChange={e => onScrub(e.target.value)}
          onMouseUp={onScrubEnd}
          onKeyUp={onScrubEnd}
        />
        <span>
          {Number.isNaN(duration) ? '0' : Math.trunc(duration / 60)}:
          {Number.isNaN(duration)
            ? '00'
            : Math.trunc(duration) - Math.trunc(duration / 60) * 60}
        </span>
      </PlayerStateWrapper>
    </Wrapper>
  );
}

const height = '5px';
const thumbHeight = 8;
const trackHeight = '16px';

// colours
const upperColor = '#edf5f9';
const lowerColor = '#0199ff';
const upperBackground = `linear-gradient(to bottom, ${upperColor}, ${upperColor}) 100% 50% / 100% ${trackHeight} no-repeat transparent`;
const lowerBackground = `linear-gradient(to bottom, ${lowerColor}, ${lowerColor}) 100% 50% / 100% ${trackHeight} no-repeat transparent`;

const makeLongShadow = (color, size) => {
  let i = 18;
  let shadow = `${i}px 0 0 ${size} ${color}`;

  for (; i < 706; i++) {
    shadow = `${shadow}, ${i}px 0 0 ${size} ${color}`;
  }

  return shadow;
};

const PlayState = styled.input`
  overflow: hidden;
  display: block;
  appearance: none;
  max-width: 700px;
  width: 100%;
  margin: 0;
  height: ${height};
  cursor: pointer;

  &:focus {
    outline: none;
  }

  &::-webkit-slider-runnable-track {
    width: 100%;
    height: ${height};
    background: ${p => p.theme.scroll.thumb};
  }

  &::-webkit-slider-thumb {
    position: relative;
    appearance: none;
    height: ${thumbHeight}px;
    width: ${thumbHeight}px;
    background: ${p => p.theme.scroll.track};
    border-radius: 5%;
    border: 0;
    top: 50%;
    transform: translateY(-50%);
    box-shadow: ${makeLongShadow(upperColor, '-10px')};
    transition: background-color 150ms;
  }

  &::-moz-range-track {
    width: 100%;
    height: ${height};
    background: ${upperBackground};
  }
  &::-moz-range-progress {
    width: 100%;
    height: ${height};
    background: ${upperBackground};
  }

  &::-moz-range-progress {
    background: ${lowerBackground};
  }

  &::-moz-range-thumb {
    appearance: none;
    margin: 0;
    height: ${thumbHeight};
    width: ${thumbHeight};
    background: ${p => p.theme.scroll.track};
    border-radius: 100%;
    border: 0;
    transition: background-color 150ms;
  }

  &::-ms-track {
    width: 100%;
    height: ${height};
    border: 0;
    /* color needed to hide track marks */
    color: transparent;
    background: transparent;
  }

  &::-ms-fill-lower {
    background: ${lowerBackground};
  }

  &::-ms-fill-upper {
    background: ${upperBackground};
  }

  &::-ms-thumb {
    appearance: none;
    height: ${thumbHeight};
    width: ${thumbHeight};
    background: ${p => p.theme.scroll.track};
    border-radius: 100%;
    border: 0;
    transition: background-color 150ms;
    /* IE Edge thinks it can support -webkit prefixes */
    top: 0;
    margin: 0;
    box-shadow: none;
  }

  &:hover,
  &:focus {
    &::-webkit-slider-thumb {
      background-color: ${p => p.theme.scroll.track};
    }
    &::-moz-range-thumb {
      background-color: ${p => p.theme.scroll.track};
    }
    &::-ms-thumb {
      background-color: ${p => p.theme.scroll.track};
    }
  }
`;

const Wrapper = styled.div`
  max-width: 600px;
  display: flex;
  flex-direction: column;
  vertical-align: center;
  width: 100%;
`;

const StateButtonsWrapper = styled.div`
  padding-left: 90px;
  padding-right: 90px;
  margin-top: auto;
  margin-bottom: 15px;
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
`;

const PlayerStateWrapper = styled.div`
  margin-bottom: auto;
  justify-content: space-evenly;
  vertical-align: center;
  display: flex;
  flex-direction: row;

  span {
    font-size: 0.9rem;
  }

  ${PlayState} {
    margin-top: 6px;
    margin-left: 2px;
    margin-right: 2px;
  }
`;

const PlayerStopDark = styled(PlayerStopDarkSvg)`
  width: 24px;
  height: 24px;
`;

const PlayerStopLight = styled(PlayerStopLightSvg)`
  width: 24px;
  height: 24px;
`;

const PlayerNextDark = styled(PlayerNextDarkSvg)`
  width: 24px;
  height: 24px;
`;

const PlayerNextLight = styled(PlayerNextLightSvg)`
  width: 24px;
  height: 24px;
`;
