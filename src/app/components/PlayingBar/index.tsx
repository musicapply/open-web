import * as React from 'react';
import styled from 'styled-components/macro';
import { StyleConstants } from 'styles/StyleConstants';
import { ThemeSwitcher } from '../ThemeSwitcher';
import { TrackInfo } from './TrackInfo';
import { useSelector } from 'react-redux';
import { selectPlayerState } from './slice/selector';
import { Player } from './Player';

export function PlayingBar() {
  const playerState = useSelector(selectPlayerState);

  return (
    <Wrapper>
      <TrackInfo track={playerState?.track!} />
      <Player state={playerState} />
      <ThemeSwitcher />
    </Wrapper>
  );
}

const Wrapper = styled.div`
  display: flex;
  width: 100%;
  min-height: ${StyleConstants.PLAYING_BAR_HEIGHT};
  max-height: ${StyleConstants.PLAYING_BAR_HEIGHT};
  background: ${p => p.theme.background};
  justify-content: space-between;
`;
