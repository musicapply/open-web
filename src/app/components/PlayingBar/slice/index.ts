import { PlayerState } from 'types/PlayerState';
import { getPlayerStateFromLocalStorage } from './utils';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { PayloadAction } from '@reduxjs/toolkit';
import { Track } from 'types/Track';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';

export const initialState: PlayerState = {
  playerState: getPlayerStateFromLocalStorage(),
};

export const slice = createSlice({
  name: 'player',
  initialState,
  reducers: {
    setTrack(state, action: PayloadAction<Track>) {
      state.playerState!.track = action.payload;
    },
  },
});

export const { actions: playerStateActions, reducer } = slice;

export const usePlayerStateSlice = () => {
  useInjectReducer({
    key: slice.name,
    reducer: slice.reducer,
  });
  return {
    actions: slice.actions,
  };
};
