import { CurrentPlayerState } from 'types/CurrentPlayerState';

const init: CurrentPlayerState = {
  track: {
    name: '',
    fileUrl: '',
    trackId: '',
    album: {
      imageUrl: '',
      albumId: '',
      releaseDate: '',
      name: '',
      artistId: '',
      description: '',
    },
    artist: {
      name: '',
      artistId: '',
      bio: '',
      imageUrl: '',
    },
  },
  loop: false,
};

export function savePlayerStateToLocalStorage(playerState: CurrentPlayerState) {
  window.localStorage &&
    localStorage.setItem('playerState', JSON.stringify(playerState));
}

export function getPlayerStateFromLocalStorage(): CurrentPlayerState {
  return window.localStorage
    ? (JSON.parse(
        <string>localStorage.getItem('playerState'),
      ) as CurrentPlayerState) || init
    : init;
}
