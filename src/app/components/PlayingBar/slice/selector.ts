import { createSelector } from '@reduxjs/toolkit';
import { RootState } from 'types';
import { initialState } from './index';

const selectDomain = (state: RootState) => state.player || initialState;

export const selectPlayerState = createSelector(
  [selectDomain],
  playerState => playerState?.playerState,
);
