import * as React from 'react';
import styled from 'styled-components/macro';
import { ReactComponent as ActiveHeartSvg } from './assets/ActiveHeartSvg.svg';
import { ReactComponent as InactiveHeartSvg } from './assets/InactiveHeartSvg.svg';
import { Track } from 'types/Track';
import { useDispatch, useSelector } from 'react-redux';
import { selectFavoriteTracks } from 'app/pages/collection/Tracks/slice/selector';
import { useFavoriteSlice } from 'app/pages/collection/Tracks/slice';

interface TrackInfoProps {
  track: Track;
}

export function TrackInfo(props: TrackInfoProps) {
  const track: Track = props.track;

  const favoriteTracks = useSelector(selectFavoriteTracks);

  const dispatch = useDispatch();
  const { actions: favoriteActions } = useFavoriteSlice();

  const removeTrackFromFavorite = (track: Track) => {
    dispatch(favoriteActions.removeTrackFromFavorite(track));
  };

  const addTrackToFavorite = (track: Track) => {
    dispatch(favoriteActions.addTrackToFavorite(track));
  };

  return (
    <Wrapper>
      <img src={track?.album.imageUrl} alt="" />
      <LikedWrapper>
        {
          // @ts-ignore
          favoriteTracks?.filter(fav => fav.track.trackId === track.trackId)
            .length > 0 ? (
            <ActiveHeartIcon onClick={e => removeTrackFromFavorite(track)} />
          ) : (
            <InactiveHeartIcon onClick={e => addTrackToFavorite(track)} />
          )
        }
      </LikedWrapper>
      <SongInfoWrapper>
        <SongName>{track?.name.toUpperCase()}</SongName>
        <SongAuthor>{track?.artist.name.toUpperCase()}</SongAuthor>
      </SongInfoWrapper>
    </Wrapper>
  );
}

const ActiveHeartIcon = styled(ActiveHeartSvg)`
  width: 24px;
  height: 24px;
`;

const InactiveHeartIcon = styled(InactiveHeartSvg)`
  width: 24px;
  height: 24px;
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  width: 350px;
`;

const LikedWrapper = styled.div`
  margin-left: 24px;
  margin-right: 28px;
  display: flex;
  align-items: center;
  height: 100%;
`;

const SongInfoWrapper = styled.div`
  vertical-align: center;
  display: flex;
  flex-direction: column;
`;

const SongName = styled.span`
  margin-top: auto;
  font-weight: bolder;
`;

const SongAuthor = styled.span`
  margin-bottom: auto;
  font-size: 0.9rem;
`;
