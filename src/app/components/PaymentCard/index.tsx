import * as React from 'react';
import styled from 'styled-components/macro';
import { PaymentMethod } from '../../../types/PaymentMethod';
import { ReactComponent as VisaPaymentsSvg } from './assets/VisaPaymentsSvg.svg';
import { ReactComponent as MasterCardSvg } from './assets/MasterCardSvg.svg';
import { ReactComponent as ActiveHeartSvg } from './assets/ActiveHeartSvg.svg';
import { ReactComponent as InactiveHeartSvg } from './assets/InactiveHeartSvg.svg';
import { ReactComponent as RedCancelSvg } from './assets/RedCancelSvg.svg';

interface PaymentCardProps {
  card: PaymentMethod;
  defaultPaymentMethodId: string | undefined;
  setDefaultPaymentMethodCallback: any;
  deletePaymentMethodCallback: any;
}

export function PaymentCard(props: PaymentCardProps) {
  const card = props.card;
  const defaultPaymentMethodId = props.defaultPaymentMethodId;
  const callback = props.setDefaultPaymentMethodCallback;
  const deleteCallback = props.deletePaymentMethodCallback;

  const cardLogo =
    card.cardBrand === 'visa' ? <VisaPaymentsLogo /> : <MasterCardLogo />;

  return (
    <Wrapper>
      <CardInfoWrapper>
        {cardLogo}
        <span>* {card.cardNumber}</span>
      </CardInfoWrapper>
      <CardActionsWrapper>
        {defaultPaymentMethodId === card.paymentMethodId ? (
          <ActiveHeartIcon />
        ) : (
          <InactiveHeartIcon onClick={() => callback(card)} />
        )}
        <DeleteButtonIcon onClick={() => deleteCallback(card)} />
      </CardActionsWrapper>
    </Wrapper>
  );
}

const ActiveHeartIcon = styled(ActiveHeartSvg)`
  width: 24px;
  height: 24px;
`;

const InactiveHeartIcon = styled(InactiveHeartSvg)`
  width: 24px;
  height: 24px;
`;

const DeleteButtonIcon = styled(RedCancelSvg)`
  width: 24px;
  height: 24px;
`;

const VisaPaymentsLogo = styled(VisaPaymentsSvg)`
  width: 165px;
  height: 67px;
`;

const MasterCardLogo = styled(MasterCardSvg)`
  width: 165px;
  height: 67px;

  svg {
    width: 100%;
  }
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  margin-right: 35px;
`;

const CardInfoWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: end;

  span {
    font-size: 1.15rem;
    font-weight: bolder;
  }
`;

const CardActionsWrapper = styled.div`
  display: flex;
  flex-direction: column;

  ${ActiveHeartIcon} {
    margin-left: 15px;
  }

  ${InactiveHeartIcon} {
    margin-left: 15px;
  }

  ${DeleteButtonIcon} {
    margin-left: 15px;
    margin-top: 15px;
  }
`;
