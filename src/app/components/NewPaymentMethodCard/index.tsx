import * as React from 'react';
import styled, { useTheme } from 'styled-components/macro';
import { useDispatch } from 'react-redux';
import CreditCardInput from 'react-credit-card-input';
import { useState } from 'react';
import { usePaymentProfileSlice } from '../../pages/User/PaymentProfile/slice';
import { AddPaymentMethodRequest } from '../../../types/AddPaymentMethodRequest';

interface NewPaymentMethodCardProps {
  closeCallback: any;
}

export function NewPaymentMethodCard(props: NewPaymentMethodCardProps) {
  const dispatch = useDispatch();

  const [cardNumber, setCardNumber] = useState('');
  const [expiry, setExpiry] = useState('');
  const [cvc, setCvc] = useState('');

  const { actions: paymentProfileActions } = usePaymentProfileSlice();

  const handleChangeCardNumber = event => {
    setCardNumber(event.target.value);
  };

  const handleChangeExpiry = event => {
    setExpiry(event.target.value);
  };

  const handleChangeCvc = event => {
    setCvc(event.target.value);
  };

  const addPaymentMethod = event => {
    const expMonth = expiry.split('/')[0].trim();
    const expYear = expiry.split('/')[1].trim();

    const request: AddPaymentMethodRequest = {
      cardNumber: cardNumber,
      expMonth: expMonth,
      expYear: expYear,
      cvc: cvc,
    };
    dispatch(paymentProfileActions.addPaymentMethod(request));
    props.closeCallback();
  };

  return (
    <Wrapper>
      <Title>Новый платежный метод</Title>
      <CreditCardInput
        cardNumberInputProps={{
          value: cardNumber,
          onChange: handleChangeCardNumber,
        }}
        cardExpiryInputProps={{ value: expiry, onChange: handleChangeExpiry }}
        cardCVCInputProps={{ value: cvc, onChange: handleChangeCvc }}
        fieldClassName="input"
      />
      <ActionsWrapper>
        <Button onClick={props.closeCallback}>Закрыть</Button>
        <Button onClick={e => addPaymentMethod(e)}>Добавить</Button>
      </ActionsWrapper>
    </Wrapper>
  );
}

const Input = styled.input`
  all: unset;
  font-size: 18px;
  height: 100%;
  width: 100%;
`;

const Button = styled.button`
  border-radius: 10px;
  height: 46px;
  min-height: 46px;
  max-height: 46px;
  background: ${p => p.theme.green};
  border: none;
  font-size: 1.25rem;
  font-weight: bolder;
  color: white;
`;

const Title = styled.h1`
  font-size: 2rem;
  font-weight: bolder;
`;

const Wrapper = styled.div`
  background: ${p => p.theme.background};
  box-shadow: 0px 0px 2px 1px rgba(0, 0, 0, 0.2);
  display: flex;
  flex-direction: column;
  padding: 15px;
  border: none;
  border-radius: 10px;
`;

const ActionsWrapper = styled.div`
  display: flex;
  flex-direction: row;
  padding-top: 15px;
  justify-content: space-between;
`;
