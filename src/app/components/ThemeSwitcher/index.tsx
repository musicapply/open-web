import * as React from 'react';
import styled from 'styled-components/macro';
import { useDispatch, useSelector } from 'react-redux';
import { selectThemeKey } from 'styles/theme/slice/selectors';
import { saveTheme } from 'styles/theme/utils';
import { themeActions } from '../../../styles/theme/slice';

import { ReactComponent as MoonIcon } from './assets/moon.svg';
import { ReactComponent as SunIcon } from './assets/sun.svg';

export function ThemeSwitcher() {
  const theme = useSelector(selectThemeKey);
  const dispatch = useDispatch();

  const toggleTheme = () => {
    if (theme === 'light') {
      const newTheme = 'dark';
      saveTheme(newTheme);
      dispatch(themeActions.changeTheme(newTheme));
    } else {
      const newTheme = 'light';
      saveTheme(newTheme);
      dispatch(themeActions.changeTheme(newTheme));
    }
  };

  return (
    <Wrapper>
      <SwitcherContainer onClick={toggleTheme}>
        <SunIcon />
        <MoonIcon />
      </SwitcherContainer>
    </Wrapper>
  );
}

export const SwitcherContainer = styled.button`
  background: ${({ theme }) => theme.gradient};
  border: 2px solid ${({ theme }) => theme.toggleBorder};
  border-radius: 30px;
  cursor: pointer;
  display: flex;
  font-size: 0.5rem;
  justify-content: space-between;
  margin: 0 auto;
  overflow: hidden;
  padding: 0.5rem;
  position: relative;
  width: 6rem;
  height: 3rem;

  svg {
    height: auto;
    width: 2.5rem;
    transition: all 0.3s linear;

    // sun icon
    &:first-child {
      transform: ${({ theme }) =>
        theme.type === 'light' ? 'translateY(0)' : 'translateY(100px)'};
    }

    // moon icon
    &:nth-child(2) {
      transform: ${({ theme }) =>
        theme.type === 'light' ? 'translateY(-100px)' : 'translateY(0)'};
    }
  }
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  width: 180px;
`;
