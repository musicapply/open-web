import * as React from 'react';
import styled, { useTheme } from 'styled-components/macro';
import { StyleConstants } from 'styles/StyleConstants';
import { media } from '../../../styles/media';

import { ReactComponent as HomeSvg } from './assets/Home.svg';
import { ReactComponent as HomeDarkSvg } from './assets/HomeDark.svg';
import { ReactComponent as BrowseSvg } from './assets/Browse.svg';
import { ReactComponent as BrowseDarkSvg } from './assets/BrowseDark.svg';
import { ReactComponent as LikedSvg } from './assets/Liked.svg';
import { ReactComponent as LikedDarkSvg } from './assets/LikedDark.svg';
import { NavLink } from 'react-router-dom';
import { rgba } from 'polished';

export function MobileAppBar() {
  const theme = useTheme();

  return (
    <Wrapper>
      <NavButton to="/" exact>
        {theme.type === 'dark' ? <HomeIconDark /> : <HomeIcon />}
      </NavButton>
      <NavButton to="/profile" exact>
        {theme.type === 'dark' ? <HomeIconDark /> : <HomeIcon />}
      </NavButton>
      <NavButton to="/collection/tracks" exact>
        {theme.type === 'dark' ? <LikedIconDark /> : <LikedIcon />}
      </NavButton>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 100%;
  min-height: ${StyleConstants.MOBILE_APP_BAR_HEIGHT};
  max-height: ${StyleConstants.MOBILE_APP_BAR_HEIGHT};
  height: ${StyleConstants.MOBILE_APP_BAR_HEIGHT};
  background: ${p => p.theme.background};
  padding-right: 10px;
  padding-left: 10px;
  justify-content: space-around;

  ${media.small`
    display: flex
  `}

  ${media.medium`
    display: none
  `}

  ${media.large`
    display: none
  `}

  ${media.xlarge`
    display: none
  `}
`;

const HomeIcon = styled(HomeSvg)`
  width: ${StyleConstants.ROOT_NAV_BAR_BUTTON_ICON_WIDTH};
  height: ${StyleConstants.ROOT_NAV_BAR_BUTTON_ICON_HEIGHT};
`;

const HomeIconDark = styled(HomeDarkSvg)`
  width: ${StyleConstants.ROOT_NAV_BAR_BUTTON_ICON_WIDTH};
  height: ${StyleConstants.ROOT_NAV_BAR_BUTTON_ICON_HEIGHT};
`;

const BrowseIcon = styled(BrowseSvg)`
  width: ${StyleConstants.ROOT_NAV_BAR_BUTTON_ICON_WIDTH};
  height: ${StyleConstants.ROOT_NAV_BAR_BUTTON_ICON_HEIGHT};
`;

const BrowseIconDark = styled(BrowseDarkSvg)`
  width: ${StyleConstants.ROOT_NAV_BAR_BUTTON_ICON_WIDTH};
  height: ${StyleConstants.ROOT_NAV_BAR_BUTTON_ICON_HEIGHT};
`;

const LikedIcon = styled(LikedSvg)`
  width: ${StyleConstants.ROOT_NAV_BAR_BUTTON_ICON_WIDTH};
  height: ${StyleConstants.ROOT_NAV_BAR_BUTTON_ICON_HEIGHT};
`;

const LikedIconDark = styled(LikedDarkSvg)`
  width: ${StyleConstants.ROOT_NAV_BAR_BUTTON_ICON_WIDTH};
  height: ${StyleConstants.ROOT_NAV_BAR_BUTTON_ICON_HEIGHT};
`;

const NavButton = styled(NavLink)`
  background: transparent;
  color: ${p => p.theme.text.primary};
  border-radius: 10px;
  border: none;
  height: 35px;
  font-size: 1.45rem;
  display: flex;
  flex-direction: row;
  text-decoration: none;

  &.active {
    background: ${p => rgba(p.theme.green, 0.15)};
  }

  &:visited {
    color: inherit;
  }

  svg {
    align-self: center;
  }
`;
