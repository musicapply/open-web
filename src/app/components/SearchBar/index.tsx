import * as React from 'react';
import styled, { useTheme } from 'styled-components/macro';
import { ReactComponent as SearchLogo } from './assets/SearchLogoLight.svg';
import { ReactComponent as SearchLogoDark } from './assets/SearchLogoDark.svg';
import { media } from 'styles/media';
import { useHistory } from 'react-router-dom';
import { useState } from 'react';

export function SearchBar() {
  const theme = useTheme();
  const history = useHistory();

  const [searchValue, setSearchValue] = useState('');

  const onInputChange = event => {
    setSearchValue(event.target.value);
  };

  const navigateToSearchPage = event => {
    event.preventDefault();
    history.push(`/search/${searchValue}`);
  };

  return (
    <Form onSubmit={e => navigateToSearchPage(e)}>
      <Input value={searchValue} onChange={onInputChange} />
      <LogoWrapper>
        {theme.type === 'dark' ? <LogoDark /> : <Logo />}
      </LogoWrapper>
    </Form>
  );
}

const Logo = styled(SearchLogo)`
  width: 24px;
  height: 24px;
`;

const LogoDark = styled(SearchLogoDark)`
  width: 24px;
  height: 24px;
`;

const Input = styled.input`
  all: unset;
  font-size: 18px;
  height: 100%;
  width: 100%;
`;

const LogoWrapper = styled.div``;

const Form = styled.form`
  width: 464px;
  height: 48px;
  background: ${p => p.theme.background};
  border-radius: 10px;
  display: none;
  font-weight: bolder;
  flex-direction: row;
  align-items: center;

  ${Input} {
    color: ${p => p.theme.text};
    padding-right: 10px;
  }

  ${LogoWrapper} {
    order: -1;
    padding-left: 20px;
    padding-right: 15px;
  }

  ${media.small`
    display: none
  `}

  ${media.medium`
    display: flex
  `}

  ${media.large`
    display: flex
  `}

  ${media.xlarge`
    display: flex
  `} //  {
  //   margin-right: 18px;
  //   padding-left: 9px;
  //   :hover {
  //     cursor: pointer;
  //   }
  // }
`;
