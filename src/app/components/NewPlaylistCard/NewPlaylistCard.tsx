import * as React from 'react';
import styled, { useTheme } from 'styled-components/macro';
import { StyleConstants } from 'styles/StyleConstants';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { usePlaylistsSlice } from '../../pages/collection/Playlists/slice';

interface NewPlaylistCardProps {
  closeCallback: any;
}

export function NewPlaylistCard(props: NewPlaylistCardProps) {
  const [name, setName] = useState('');

  const dispatch = useDispatch();
  const { actions: playlistsActions } = usePlaylistsSlice();

  const handleChangeInput = event => {
    setName(event.target.value);
  };

  const createPlaylist = () => {
    dispatch(playlistsActions.createPlaylist(name));
    props.closeCallback();
  };

  return (
    <Wrapper>
      <Title>Новый плейлист</Title>
      <Input value={name} onChange={handleChangeInput} type="text" />
      <ActionsWrapper>
        <Button onClick={props.closeCallback}>Закрыть</Button>
        <Button onClick={createPlaylist}>Добавить</Button>
      </ActionsWrapper>
    </Wrapper>
  );
}

const Input = styled.input`
  all: unset;
  font-size: 18px;
  height: 100%;
  width: 100%;
`;

const Button = styled.button`
  border-radius: 10px;
  height: 46px;
  min-height: 46px;
  max-height: 46px;
  background: ${p => p.theme.green};
  border: none;
  font-size: 1.25rem;
  font-weight: bolder;
  color: white;
`;

const Title = styled.h1`
  font-size: 2rem;
  font-weight: bolder;
`;

const Wrapper = styled.div`
  background: ${p => p.theme.background};
  box-shadow: 0px 0px 2px 1px rgba(0, 0, 0, 0.2);
  display: flex;
  flex-direction: column;
  padding: 15px;
  border: none;
  border-radius: 10px;
`;

const ActionsWrapper = styled.div`
  display: flex;
  flex-direction: row;
  padding-top: 15px;
  justify-content: space-between;
`;
