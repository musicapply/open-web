import * as React from 'react';
import styled from 'styled-components/macro';

interface ImageProps {
  width: number;
  height: number;
  imageUrl: string | undefined;
}

export function ImageViewer(props: ImageProps) {
  return (
    <Wrapper>
      <Img src={props.imageUrl} width={props.width} height={props.height} />
    </Wrapper>
  );
}

const Wrapper = styled.div.attrs(
  (props: { width: number; height: number }) => props,
)`
  display: block;
  min-width: ${props => props.width};
  min-height: ${props => props.height};
  max-width: ${props => props.width};
  max-height: ${props => props.height};
`;

const Img = styled.img`
  border-radius: 10px;
`;
