import * as React from 'react';
import styled from 'styled-components/macro';

import { ImageViewer } from 'app/components/ImageViewer/ImageViewer';
import { useHistory } from 'react-router-dom';
import { AlbumShort } from 'types/AlbumShort';

interface ArtistAlbumCardProps {
  album: AlbumShort;
}

export function ArtistAlbumCard(props: ArtistAlbumCardProps) {
  const history = useHistory();

  const album: AlbumShort = props.album;

  return (
    <Wrapper onClick={e => history.push(`/album/${album.albumId}`)}>
      <ImageViewer width={200} height={200} imageUrl={album.imageUrl} />
      <AlbumName>{album.name}</AlbumName>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  margin-right: 75px;
  margin-bottom: 35px;
  max-width: 200px;
  display: flex;
  flex-direction: column;
`;

const AlbumName = styled.p`
  margin-top: 5px;
  margin-bottom: 0px;
  font-size: 1.25rem;
  font-weight: bolder;
`;

const ArtistName = styled.p`
  margin-top: 0px;
  margin-bottom: 0px;
  font-size: 1.15rem;
  font-weight: bolder;
  color: ${p => p.theme.table.border};
`;
