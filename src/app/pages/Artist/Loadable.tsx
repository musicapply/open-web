import { lazyLoad } from 'utils/loadable';

export const ArtistPage = lazyLoad(
  () => import('./index'),
  module => module.ArtistPage,
);
