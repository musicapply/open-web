import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { useParams } from 'react-router';
import styled from 'styled-components/macro';
import { media } from 'styles/media';
import { useEffect, useState } from 'react';
import { ArtistFull } from 'types/ArtistFull';
import { getArtistById } from 'utils/api/Artist';
import { ImageViewer } from 'app/components/ImageViewer/ImageViewer';
import { useDispatch, useSelector } from 'react-redux';
import { selectLibraryArtists } from '../collection/Artists/slice/selector';

import { ReactComponent as ActiveHeartSvg } from './assets/ActiveHeartSvg.svg';
import { ReactComponent as InactiveHeartSvg } from './assets/InactiveHeartSvg.svg';
import { useLibraryArtistSlice } from '../collection/Artists/slice';
import { ArtistAlbumCard } from './ArtistAlbum';

export function ArtistPage(props) {
  const [artist, setArtist] = useState<ArtistFull | null>(null);

  // @ts-ignore
  const { artistId } = useParams();
  const libraryArtists = useSelector(selectLibraryArtists);
  const { actions: libraryArtistsActions } = useLibraryArtistSlice();
  const dispatch = useDispatch();

  const useEffectOnMount = (effect: React.EffectCallback) => {
    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(effect, []);
  };

  useEffectOnMount(() => {
    // @ts-ignore
    getArtistById(artistId).then(res => setArtist(res));
  });

  const removeArtistFromLibrary = (artist: ArtistFull) => {
    dispatch(libraryArtistsActions.removeArtistFromLibrary(artist));
  };

  const addArtistToLibrary = (artist: ArtistFull) => {
    dispatch(libraryArtistsActions.addArtistToLibrary(artist));
  };

  return (
    <>
      <Helmet>
        <title>{artist?.name}</title>
        <meta name="description" content={`${artist?.name}`} />
      </Helmet>
      <PageWrapper>
        <PageTitle>{artist?.name}</PageTitle>
        <ArtistInfoWrapper>
          <ImageViewer imageUrl={artist?.imageUrl} width={200} height={200} />
          <ArtistActionsWrapper>
            {
              // @ts-ignore
              libraryArtists?.filter(
                libArtist => libArtist.artist.artistId === artist?.artistId,
              ).length > 0 ? (
                <ActiveHeartIcon
                  onClick={e => removeArtistFromLibrary(artist!)}
                />
              ) : (
                <InactiveHeartIcon onClick={e => addArtistToLibrary(artist!)} />
              )
            }
          </ArtistActionsWrapper>
        </ArtistInfoWrapper>
        <AlbumsSectionWrapper>
          <AlbumsTitle>Альбомы</AlbumsTitle>
          <AlbumsSection>
            {artist?.albums.map(album => (
              <ArtistAlbumCard key={album.albumId} album={album} />
            ))}
          </AlbumsSection>
        </AlbumsSectionWrapper>
      </PageWrapper>
    </>
  );
}

const PageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding-left: 10px;
  padding-right: 10px;
  width: 100%;

  ${media.small`
    padding-left: 10px;
    padding-right: 10px;
  `}

  ${media.medium`
    padding-left: 54px;
    padding-right: 54px;
  `}

  ${media.large`
    padding-left: 54px;
    padding-right: 54px;
  `}

  ${media.xlarge`
    padding-left: 54px;
    padding-right: 54px;
  `}
`;

const PageTitle = styled.h1`
  font-size: 2rem;
  font-weight: bolder;
`;

const ArtistInfoWrapper = styled.div`
  display: flex;
  flex-direction: row;
`;

const ArtistActionsWrapper = styled.div`
  padding-left: 15px;
  display: flex;
  flex-direction: column;
`;

const AlbumsSectionWrapper = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  flex-wrap: wrap;
`;

const AlbumsTitle = styled.h1`
  font-size: 1.5rem;
  font-weight: bolder;
`;

const AlbumsSection = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  flex-wrap: wrap;
`;

const ActiveHeartIcon = styled(ActiveHeartSvg)`
  width: 24px;
  height: 24px;
`;

const InactiveHeartIcon = styled(InactiveHeartSvg)`
  width: 24px;
  height: 24px;
`;
