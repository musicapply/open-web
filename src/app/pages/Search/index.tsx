import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import styled from 'styled-components/macro';
import { media } from 'styles/media';
import { useParams } from 'react-router';
import { useEffect, useState } from 'react';
import { ArtistFull } from 'types/ArtistFull';
import { searchArtistsLikeName } from 'utils/api/Artist';
import { ArtistCard } from './ArtistCard';
import { Album } from 'types/Album';
import { searchAlbumsLikeName } from 'utils/api/Album';
import { searchTracksLikeName } from 'utils/api/Track';
import { AlbumCard } from './AlbumCard';
import { TracksTable } from './TrackTable';
import { Track } from 'types/Track';

export function SearchPage() {
  // @ts-ignore
  const { searchArg } = useParams();

  const [artists, setArtists] = useState<ArtistFull[] | null>(null);
  const [albums, setAlbums] = useState<Album[] | null>(null);
  const [tracks, setTracks] = useState<Track[] | null>(null);

  const useEffectOnMount = (effect: React.EffectCallback) => {
    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(effect, []);
  };

  useEffectOnMount(() => {
    // @ts-ignore
    searchArtistsLikeName(searchArg).then(res => setArtists(res));
    // @ts-ignore
    searchAlbumsLikeName(searchArg).then(res => setAlbums(res));

    return () => {
      setArtists(null);
      setAlbums(null);
    };
  });

  useEffect(() => {
    // @ts-ignore
    searchArtistsLikeName(searchArg).then(res => setArtists(res));
    // @ts-ignore
    searchAlbumsLikeName(searchArg).then(res => setAlbums(res));
    // @ts-ignore
    searchTracksLikeName(searchArg).then(res => setTracks(res));

    return () => {
      setArtists(null);
      setAlbums(null);
      setTracks(null);
    };
  }, [searchArg]);

  return (
    <>
      <Helmet>
        <title>Поиск</title>
        <meta name="description" content="Поиск" />
      </Helmet>
      <PageWrapper>
        <PageTitle>Поиск - {searchArg}</PageTitle>
        {artists?.length !== 0 ? (
          <>
            <ArtistsTitle>Исполнители</ArtistsTitle>
            <ArtistsSection>
              {artists?.map(artist => (
                <ArtistCard key={artist.artistId} artist={artist} />
              ))}
            </ArtistsSection>
          </>
        ) : null}
        {albums?.length !== 0 ? (
          <>
            <AlbumsTitle>Альбомы</AlbumsTitle>
            <AlbumsSection>
              {albums?.map(album => (
                <AlbumCard key={album.albumId} album={album} />
              ))}
            </AlbumsSection>
          </>
        ) : null}
        {tracks?.length !== 0 ? (
          <>
            <TracksTitle>Композиции</TracksTitle>
            <TracksWrapper>
              <TracksTable tracks={tracks!} />
            </TracksWrapper>
          </>
        ) : null}
      </PageWrapper>
    </>
  );
}

const TracksWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: 35px;
`;

const ArtistsTitle = styled.h1`
  font-size: 1.5rem;
  font-weight: bolder;
`;

const AlbumsTitle = styled.h1`
  font-size: 1.5rem;
  font-weight: bolder;
`;

const TracksTitle = styled.h1`
  font-size: 1.5rem;
  font-weight: bolder;
`;

const PageTitle = styled.h1`
  font-size: 2rem;
  font-weight: bolder;
`;

const PageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding-left: 10px;
  padding-right: 10px;
  width: 100%;

  ${media.small`
    padding-left: 10px;
    padding-right: 10px;
  `}

  ${media.medium`
    padding-left: 54px;
    padding-right: 54px;
  `}

  ${media.large`
    padding-left: 54px;
    padding-right: 54px;
  `}

  ${media.xlarge`
    padding-left: 54px;
    padding-right: 54px;
  `}
`;

const ArtistsSection = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  flex-wrap: wrap;
`;

const AlbumsSection = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  flex-wrap: wrap;
`;
