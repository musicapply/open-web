import * as React from 'react';
import styled from 'styled-components/macro';
import { useHistory } from 'react-router-dom';
import { ImageViewer } from 'app/components/ImageViewer/ImageViewer';
import { ArtistFull } from 'types/ArtistFull';

interface ArtistCardProps {
  artist: ArtistFull;
}

export function ArtistCard(props: ArtistCardProps) {
  const history = useHistory();

  const artist: ArtistFull = props.artist;

  return (
    <Wrapper onClick={e => history.push(`/artist/${artist.artistId}`)}>
      <ImageViewer width={200} height={200} imageUrl={artist.imageUrl} />
      <ArtistName>{artist.name}</ArtistName>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  margin-right: 75px;
  margin-bottom: 35px;
  max-width: 200px;
  display: flex;
  flex-direction: column;
`;

const ArtistName = styled.p`
  margin-top: 5px;
  margin-bottom: 0px;
  font-size: 1.25rem;
  font-weight: bolder;
`;
