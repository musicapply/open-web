import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import styled from 'styled-components/macro';
import { media } from '../../../styles/media';
import { LikedCard } from './LikedCard';
import { Card } from './Card';

export function HomePage() {
  return (
    <>
      <Helmet>
        <title>Главная</title>
        <meta name="description" content="A Boilerplate application homepage" />
      </Helmet>
      <PageWrapper>
        <PageTitle>Главная</PageTitle>
        <LinksWrapper>
          <LikedCard />
          <Card name="Исполнители" path="/collection/artists" />
          <Card name="Альбомы" path="/collection/albums" />
        </LinksWrapper>
      </PageWrapper>
    </>
  );
}

const LinksWrapper = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  flex-wrap: wrap;
`;

const PageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding-left: 10px;
  padding-right: 10px;
  width: 100%;

  ${media.small`
    padding-left: 10px;
    padding-right: 10px;
  `}

  ${media.medium`
    padding-left: 54px;
    padding-right: 54px;
  `}

  ${media.large`
    padding-left: 54px;
    padding-right: 54px;
  `}

  ${media.xlarge`
    padding-left: 54px;
    padding-right: 54px;
  `}
`;

const PageTitle = styled.h1`
  font-size: 2rem;
  font-weight: bolder;
`;
