import * as React from 'react';
import styled from 'styled-components/macro';
import { useHistory } from 'react-router-dom';
import { ReactComponent as LikedSvg } from '../assets/Liked.svg';
import { ImageViewer } from '../../../components/ImageViewer/ImageViewer';

export function Card(props) {
  const history = useHistory();

  return (
    <Wrapper onClick={e => history.push(props.path)}>
      <ImageViewer
        width={200}
        height={200}
        imageUrl={require('../assets/logo512.png')}
      />
      <LikedTitle>{props.name}</LikedTitle>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  margin-right: 75px;
  margin-bottom: 35px;
  max-width: 200px;
  display: flex;
  flex-direction: column;
`;

const LikedTitle = styled.p`
  margin-top: 5px;
  margin-bottom: 0px;
  font-size: 1.25rem;
  font-weight: bolder;
`;

const LikedIcon = styled(LikedSvg)`
  width: 200px;
  height: 200px;
`;
