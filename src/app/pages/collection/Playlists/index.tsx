import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import styled from 'styled-components/macro';
import { media } from 'styles/media';

export function PlaylistsPage() {
  return (
    <>
      <Helmet>
        <title>Плейлисты</title>
        <meta name="description" content="Плейлисты" />
      </Helmet>
      <PageWrapper>
        <PageTitle>Плейлисты</PageTitle>
        <PlaylistsSection></PlaylistsSection>
      </PageWrapper>
    </>
  );
}

const PageTitle = styled.h1`
  font-size: 2rem;
  font-weight: bolder;
`;

const PageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding-left: 10px;
  padding-right: 10px;
  width: 100%;

  ${media.small`
    padding-left: 10px;
    padding-right: 10px;
  `}

  ${media.medium`
    padding-left: 54px;
    padding-right: 54px;
  `}

  ${media.large`
    padding-left: 54px;
    padding-right: 54px;
  `}

  ${media.xlarge`
    padding-left: 54px;
    padding-right: 54px;
  `}
`;

const PlaylistsSection = styled.div`
  margin-top: 50px;
  display: flex;
  flex-direction: column;
  width: 100%;
`;
