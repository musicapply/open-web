import { Playlist } from 'types/Playlist';

export function savePlaylistsToLocalStorage(playlists: Playlist[]) {
  window.localStorage &&
    localStorage.setItem('playlists', JSON.stringify(playlists));
}

export function getPlaylistsFromLocalStorage(): Playlist[] | null {
  return window.localStorage
    ? (JSON.parse(<string>localStorage.getItem('playlists')) as Playlist[]) ||
        null
    : null;
}
