import { PlaylistsState } from 'types/PlaylistsState';
import { getPlaylistsFromLocalStorage } from './utils';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { PayloadAction } from '@reduxjs/toolkit';
import { Playlist } from 'types/Playlist';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { playlistsSaga } from './saga';
import { AddTrackToPlaylistRequest } from '../../../../../types/AddTrackToPlaylistRequest';
import { RemoveTrackFromPlaylistRequest } from '../../../../../types/RemoveTrackFromPlaylist';

export const initialState: PlaylistsState = {
  playlists: getPlaylistsFromLocalStorage() || null,
};

export const slice = createSlice({
  name: 'playlists',
  initialState,
  reducers: {
    loadPlaylists(state) {},
    playlistsLoaded(state, action: PayloadAction<Playlist[]>) {
      state.playlists = action.payload;
    },
    createPlaylist(state, action: PayloadAction<string>) {},
    addTrackToPlaylist(
      state,
      action: PayloadAction<AddTrackToPlaylistRequest>,
    ) {},
    removeTrackFromPlaylist(
      state,
      action: PayloadAction<RemoveTrackFromPlaylistRequest>,
    ) {},
    removePlaylist(state, action: PayloadAction<string>) {},
  },
});

export const { actions: playlistsActions, reducer } = slice;

export const usePlaylistsSlice = () => {
  useInjectReducer({
    key: slice.name,
    reducer: slice.reducer,
  });
  useInjectSaga({
    key: slice.name,
    saga: playlistsSaga,
  });
  return {
    actions: slice.actions,
  };
};
