import { createSelector } from '@reduxjs/toolkit';
import { RootState } from 'types';
import { initialState } from './index';

const selectDomain = (state: RootState) => state.playlists || initialState;

export const selectPlaylists = createSelector(
  [selectDomain],
  playlistsState => playlistsState?.playlists,
);
