import {
  call,
  put,
  select,
  takeLatest,
  takeEvery,
  delay,
} from 'redux-saga/effects';
import { playlistsActions as actions } from '.';
import { getAccessTokenFromLocalStorage } from 'utils/tokenUtils';
import { request } from 'utils/request';
import { PayloadAction } from '@reduxjs/toolkit';
import { Playlist } from 'types/Playlist';
import { AddTrackToPlaylistRequest } from '../../../../../types/AddTrackToPlaylistRequest';
import { RemoveTrackFromPlaylistRequest } from '../../../../../types/RemoveTrackFromPlaylist';

export function* getSavesPlaylists() {
  const requestUrl: string =
    'https://api.musicapply.ru/collection/playlists/all';
  const accessToken: string | null = getAccessTokenFromLocalStorage();

  const playlists: Playlist[] = yield call(request, requestUrl, {
    headers: {
      Authorization: `Bearer ${accessToken}`,
      'X-Access-Token': `${accessToken}`,
    },
  });

  yield put(actions.playlistsLoaded(playlists));
}

export function* createPlaylist(action: PayloadAction<string>) {
  const accessToken: string | null = getAccessTokenFromLocalStorage();
  const requestUrl: string = `https://api.musicapply.ru/collection/playlists/create/${action.payload}`;

  try {
    const response: any = yield call(request, requestUrl, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${accessToken}`,
        'X-Access-Token': `${accessToken}`,
      },
    });
  } catch (err: any) {
    console.error('Error parse JSON');
  }

  yield put(actions.loadPlaylists());
}

export function* removeTrackFromPlaylist(
  action: PayloadAction<RemoveTrackFromPlaylistRequest>,
) {
  const accessToken: string | null = getAccessTokenFromLocalStorage();
  const requestUrl: string = `https://api.musicapply.ru/collection/playlists/removeTrack`;

  try {
    const response: any = yield call(request, requestUrl, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${accessToken}`,
        'X-Access-Token': `${accessToken}`,
      },
      body: JSON.stringify(action.payload),
    });
  } catch (err: any) {
    console.error('Error parse JSON');
  }

  yield put(actions.loadPlaylists());
}

export function* addTrackToPlaylist(
  action: PayloadAction<AddTrackToPlaylistRequest>,
) {
  const accessToken: string | null = getAccessTokenFromLocalStorage();
  const requestUrl: string = `https://api.musicapply.ru/collection/playlists/addTrack`;

  try {
    const response: any = yield call(request, requestUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${accessToken}`,
        'X-Access-Token': `${accessToken}`,
      },
      body: JSON.stringify(action.payload),
    });
  } catch (err: any) {
    console.error('Error parse JSON');
  }

  yield put(actions.loadPlaylists());
}

export function* removePlaylist(action: PayloadAction<string>) {
  const accessToken: string | null = getAccessTokenFromLocalStorage();
  const requestUrl: string = `https://api.musicapply.ru/collection/playlists/removePlaylist/${action.payload}`;

  try {
    const response: any = yield call(request, requestUrl, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${accessToken}`,
        'X-Access-Token': `${accessToken}`,
      },
    });
  } catch (err: any) {
    console.error('Error parse JSON');
  }

  yield put(actions.loadPlaylists());
}

export function* playlistsSaga() {
  yield takeLatest(actions.loadPlaylists.type, getSavesPlaylists);
  yield takeEvery(actions.createPlaylist.type, createPlaylist);
  yield takeEvery(actions.addTrackToPlaylist.type, addTrackToPlaylist);
  yield takeEvery(
    actions.removeTrackFromPlaylist.type,
    removeTrackFromPlaylist,
  );
  yield takeEvery(actions.removePlaylist.type, removePlaylist);
}
