import { lazyLoad } from 'utils/loadable';

export const PlaylistsPage = lazyLoad(
  () => import('./index'),
  module => module.PlaylistsPage,
);
