import { LibraryAlbumsState } from 'types/LibraryAlbumsState';
import { getLibraryAlbumsFromLocalStorage } from './utils';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { PayloadAction } from '@reduxjs/toolkit';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { LibraryAlbumRecord } from 'types/LibraryAlbumRecord';
import { libraryAlbumsSaga } from './saga';
import { Album } from 'types/Album';

export const initialState: LibraryAlbumsState = {
  libraryAlbums: getLibraryAlbumsFromLocalStorage() || null,
};

export const slice = createSlice({
  name: 'libraryAlbums',
  initialState,
  reducers: {
    loadAlbums(state) {},
    albumsLoaded(state, action: PayloadAction<LibraryAlbumRecord[]>) {
      state.libraryAlbums = action.payload;
    },
    removeAlbumFromLibrary(
      state,
      action: PayloadAction<LibraryAlbumRecord | Album>,
    ) {},
    addAlbumToLibrary(
      state,
      action: PayloadAction<LibraryAlbumRecord | Album>,
    ) {},
  },
});

export const { actions: libraryAlbumsActions, reducer } = slice;

export const useLibraryAlbumsSlice = () => {
  useInjectReducer({
    key: slice.name,
    reducer: slice.reducer,
  });
  useInjectSaga({
    key: slice.name,
    saga: libraryAlbumsSaga,
  });
  return {
    actions: slice.actions,
  };
};
