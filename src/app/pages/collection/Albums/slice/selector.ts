import { createSelector } from '@reduxjs/toolkit';
import { RootState } from 'types';
import { initialState } from './index';

const selectDomain = (state: RootState) => state.libraryAlbums || initialState;

export const selectLibraryAlbums = createSelector(
  [selectDomain],
  libraryAlbums => libraryAlbums?.libraryAlbums,
);
