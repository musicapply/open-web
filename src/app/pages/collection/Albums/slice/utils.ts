import { LibraryAlbumRecord } from 'types/LibraryAlbumRecord';

export function saveLibraryAlbumsToLocalStorage(albums: LibraryAlbumRecord[]) {
  window.localStorage && localStorage.setItem('albums', JSON.stringify(albums));
}

export function getLibraryAlbumsFromLocalStorage():
  | LibraryAlbumRecord[]
  | null {
  return window.localStorage
    ? (JSON.parse(
        <string>localStorage.getItem('albums'),
      ) as LibraryAlbumRecord[]) || null
    : null;
}
