import {
  call,
  put,
  select,
  takeLatest,
  takeEvery,
  delay,
} from 'redux-saga/effects';
import { libraryAlbumsActions as actions } from '.';
import { getAccessTokenFromLocalStorage } from 'utils/tokenUtils';
import { request } from 'utils/request';
import { PayloadAction } from '@reduxjs/toolkit';
import { LibraryAlbumRecord } from 'types/LibraryAlbumRecord';
import { Album } from 'types/Album';

export function* getLibraryAlbums() {
  const requestUrl: string = 'https://api.musicapply.ru/collection/albums/all';
  const accessToken: string | null = getAccessTokenFromLocalStorage();

  const libraryAlbums: LibraryAlbumRecord[] = yield call(request, requestUrl, {
    headers: {
      Authorization: `Bearer ${accessToken}`,
      'X-Access-Token': `${accessToken}`,
    },
  });

  yield put(actions.albumsLoaded(libraryAlbums));
}

export function* deleteAlbumFromLibrary(
  action: PayloadAction<LibraryAlbumRecord | Album>,
) {
  const album: LibraryAlbumRecord | Album = yield action.payload;
  const accessToken: string | null = getAccessTokenFromLocalStorage();

  let requestUrl: string = '';

  if ('album' in album) {
    requestUrl = `https://api.musicapply.ru/collection/albums/remove/${album.album.albumId}`;
  } else {
    requestUrl = `https://api.musicapply.ru/collection/albums/remove/${album.albumId}`;
  }

  try {
    const response: any = yield call(request, requestUrl, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${accessToken}`,
        'X-Access-Token': `${accessToken}`,
      },
    });
  } catch (err: any) {
    console.error('Error parse JSON');
  }

  yield put(actions.loadAlbums());
}

export function* addAlbumToLibrary(
  action: PayloadAction<LibraryAlbumRecord | Album>,
) {
  const album: LibraryAlbumRecord | Album = yield action.payload;
  const accessToken: string | null = getAccessTokenFromLocalStorage();

  let requestUrl: string = '';

  if ('album' in album) {
    requestUrl = `https://api.musicapply.ru/collection/albums/add/${album.album.albumId}`;
  } else {
    requestUrl = `https://api.musicapply.ru/collection/albums/add/${album.albumId}`;
  }

  try {
    const response: any = yield call(request, requestUrl, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${accessToken}`,
        'X-Access-Token': `${accessToken}`,
      },
    });
  } catch (err: any) {
    console.error('Error parse JSON');
  }

  yield put(actions.loadAlbums());
}

export function* libraryAlbumsSaga() {
  yield takeLatest(actions.loadAlbums.type, getLibraryAlbums);
  yield takeEvery(actions.removeAlbumFromLibrary.type, deleteAlbumFromLibrary);
  yield takeEvery(actions.addAlbumToLibrary.type, addAlbumToLibrary);
}
