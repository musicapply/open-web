import { lazyLoad } from 'utils/loadable';

export const AlbumsPage = lazyLoad(
  () => import('./index'),
  module => module.AlbumsPage,
);
