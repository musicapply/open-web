import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import styled from 'styled-components/macro';
import { media } from 'styles/media';
import { useLibraryAlbumsSlice } from './slice';
import { useDispatch, useSelector } from 'react-redux';
import { selectLibraryAlbums } from './slice/selector';
import { useEffect } from 'react';
import { AlbumCard } from './AlbumCard';

export function AlbumsPage() {
  const dispatch = useDispatch();
  const { actions: libraryAlbumsActions } = useLibraryAlbumsSlice();
  const libraryAlbums = useSelector(selectLibraryAlbums);

  const useEffectOnMount = (effect: React.EffectCallback) => {
    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(effect, []);
  };

  useEffectOnMount(() => {
    dispatch(libraryAlbumsActions.loadAlbums());
  });

  // @ts-ignore
  return (
    <>
      <Helmet>
        <title>Альбомы</title>
        <meta name="description" content="Альбомы" />
      </Helmet>
      <PageWrapper>
        <PageTitle>Альбомы</PageTitle>
        <AlbumsSection>
          {
            // @ts-ignore
            libraryAlbums?.map(album => (
              <AlbumCard key={album.id} album={album} />
            ))
          }
        </AlbumsSection>
      </PageWrapper>
    </>
  );
}

const PageTitle = styled.h1`
  font-size: 2rem;
  font-weight: bolder;
`;

const PageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding-left: 10px;
  padding-right: 10px;
  width: 100%;

  ${media.small`
    padding-left: 10px;
    padding-right: 10px;
  `}

  ${media.medium`
    padding-left: 54px;
    padding-right: 54px;
  `}

  ${media.large`
    padding-left: 54px;
    padding-right: 54px;
  `}

  ${media.xlarge`
    padding-left: 54px;
    padding-right: 54px;
  `}
`;

const AlbumsSection = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  flex-wrap: wrap;
`;
