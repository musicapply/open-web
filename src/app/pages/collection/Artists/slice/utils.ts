import { LibraryArtistRecord } from 'types/LibraryArtistRecord';

export function saveLibraryArtistsToLocalStorage(
  artists: LibraryArtistRecord[],
) {
  window.localStorage &&
    localStorage.setItem('artists', JSON.stringify(artists));
}

export function getLibraryArtistsFromLocalStorage():
  | LibraryArtistRecord[]
  | null {
  return window.localStorage
    ? (JSON.parse(
        <string>localStorage.getItem('artists'),
      ) as LibraryArtistRecord[]) || null
    : null;
}
