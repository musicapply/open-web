import { createSelector } from '@reduxjs/toolkit';
import { RootState } from 'types';
import { initialState } from './index';

const selectDomain = (state: RootState) => state.libraryArtists || initialState;

export const selectLibraryArtists = createSelector(
  [selectDomain],
  libraryArtists => libraryArtists?.libraryArtists,
);
