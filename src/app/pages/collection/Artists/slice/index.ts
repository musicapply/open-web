import { LibraryArtistsState } from 'types/LibraryArtistsState';
import { getLibraryArtistsFromLocalStorage } from './utils';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { PayloadAction } from '@reduxjs/toolkit';
import { LibraryArtistRecord } from 'types/LibraryArtistRecord';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { libraryArtistsSaga } from './saga';
import { ArtistFull } from 'types/ArtistFull';
import { ArtistShort } from 'types/ArtistShort';

export const initialState: LibraryArtistsState = {
  libraryArtists: getLibraryArtistsFromLocalStorage() || null,
};

export const slice = createSlice({
  name: 'libraryArtists',
  initialState,
  reducers: {
    loadArtists(state) {},
    artistsLoaded(state, action: PayloadAction<LibraryArtistRecord[]>) {
      state.libraryArtists = action.payload;
    },
    addArtistToLibrary(
      state,
      action: PayloadAction<ArtistFull | ArtistShort>,
    ) {},
    removeArtistFromLibrary(
      state,
      action: PayloadAction<ArtistFull | ArtistShort>,
    ) {},
  },
});

export const { actions: libraryArtistsActions, reducer } = slice;

export const useLibraryArtistSlice = () => {
  useInjectReducer({
    key: slice.name,
    reducer: slice.reducer,
  });
  useInjectSaga({
    key: slice.name,
    saga: libraryArtistsSaga,
  });
  return {
    actions: slice.actions,
  };
};
