import {
  call,
  put,
  select,
  takeLatest,
  takeEvery,
  delay,
} from 'redux-saga/effects';
import { libraryArtistsActions as actions } from '.';
import { getAccessTokenFromLocalStorage } from 'utils/tokenUtils';
import { request } from 'utils/request';
import { PayloadAction } from '@reduxjs/toolkit';
import { LibraryArtistRecord } from 'types/LibraryArtistRecord';
import { ArtistShort } from 'types/ArtistShort';
import { ArtistFull } from 'types/ArtistFull';

export function* getLibraryArtists() {
  const requestUrl: string = 'https://api.musicapply.ru/collection/artists/all';
  const accessToken: string | null = getAccessTokenFromLocalStorage();

  const libraryArtists: LibraryArtistRecord[] = yield call(
    request,
    requestUrl,
    {
      headers: {
        Authorization: `Bearer ${accessToken}`,
        'X-Access-Token': `${accessToken}`,
      },
    },
  );

  yield put(actions.artistsLoaded(libraryArtists));
}

export function* addArtistToLibrary(
  action: PayloadAction<ArtistFull | ArtistShort>,
) {
  const artist: ArtistFull | ArtistShort = yield action.payload;
  const accessToken: string | null = getAccessTokenFromLocalStorage();

  const requestUrl: string = `https://api.musicapply.ru/collection/artists/add/${artist.artistId}`;

  try {
    const response: any = yield call(request, requestUrl, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${accessToken}`,
        'X-Access-Token': `${accessToken}`,
      },
    });
  } catch (err: any) {
    console.error('Error parse JSON');
  }

  yield put(actions.loadArtists());
}

export function* deleteArtistFromLibrary(
  action: PayloadAction<ArtistFull | ArtistShort>,
) {
  const artist: ArtistFull | ArtistShort = yield action.payload;
  const accessToken: string | null = getAccessTokenFromLocalStorage();

  const requestUrl: string = `https://api.musicapply.ru/collection/artists/remove/${artist.artistId}`;

  try {
    const response: any = yield call(request, requestUrl, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${accessToken}`,
        'X-Access-Token': `${accessToken}`,
      },
    });
  } catch (err: any) {
    console.error('Error parse JSON');
  }

  yield put(actions.loadArtists());
}

export function* libraryArtistsSaga() {
  yield takeLatest(actions.loadArtists.type, getLibraryArtists);
  yield takeEvery(
    actions.removeArtistFromLibrary.type,
    deleteArtistFromLibrary,
  );
  yield takeEvery(actions.addArtistToLibrary.type, addArtistToLibrary);
}
