import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import styled from 'styled-components/macro';
import { media } from 'styles/media';
import { useLibraryArtistSlice } from './slice';
import { useDispatch, useSelector } from 'react-redux';
import { selectLibraryArtists } from './slice/selector';
import { ArtistCard } from './ArtistCard';
import { useEffect } from 'react';

export function ArtistsPage() {
  const libraryArtists = useSelector(selectLibraryArtists);
  const dispatch = useDispatch();
  const { actions: libraryArtistsActions } = useLibraryArtistSlice();

  const useEffectOnMount = (effect: React.EffectCallback) => {
    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(effect, []);
  };

  useEffectOnMount(() => {
    dispatch(libraryArtistsActions.loadArtists());
  });

  return (
    <>
      <Helmet>
        <title>Исполнители</title>
        <meta name="description" content="Исполнители" />
      </Helmet>
      <PageWrapper>
        <PageTitle>Исполнители</PageTitle>
        <ArtistsSection>
          {libraryArtists?.map(artist => (
            <ArtistCard key={artist.id} artist={artist} />
          ))}
        </ArtistsSection>
      </PageWrapper>
    </>
  );
}

const PageTitle = styled.h1`
  font-size: 2rem;
  font-weight: bolder;
`;

const PageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding-left: 10px;
  padding-right: 10px;
  width: 100%;

  ${media.small`
    padding-left: 10px;
    padding-right: 10px;
  `}

  ${media.medium`
    padding-left: 54px;
    padding-right: 54px;
  `}

  ${media.large`
    padding-left: 54px;
    padding-right: 54px;
  `}

  ${media.xlarge`
    padding-left: 54px;
    padding-right: 54px;
  `}
`;

const ArtistsSection = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  flex-wrap: wrap;
`;
