import * as React from 'react';
import styled, { useTheme } from 'styled-components/macro';
import { ReactComponent as ActiveHeartSvg } from './assets/ActiveHeartSvg.svg';
import { FavoriteTrackRecord } from 'types/FavoriteTrackRecord';
import { useDispatch, useSelector } from 'react-redux';
import { useFavoriteSlice } from '../slice';
import { useEffect, useState } from 'react';
import { selectShowContextMenu } from '../../../../components/ContextMenu/slice/selector';
import { useContextMenuSlice } from '../../../../components/ContextMenu/slice';

import {
  useParams,
  useLocation,
  useHistory,
  useRouteMatch,
} from 'react-router-dom';
import { usePlayerStateSlice } from '../../../../components/PlayingBar/slice';
import { Track } from '../../../../../types/Track';
import { selectPlayerState } from '../../../../components/PlayingBar/slice/selector';
import { rgba } from 'polished';
import { selectFavoriteTracks } from '../slice/selector';
import { selectPlaylists } from '../../Playlists/slice/selector';
import { usePlaylistsSlice } from '../../Playlists/slice';
import { AddTrackToPlaylistRequest } from '../../../../../types/AddTrackToPlaylistRequest';

interface TrackRowProps {
  track: FavoriteTrackRecord;
}

export function TrackRow(props: TrackRowProps) {
  const favoriteTracks = useSelector(selectFavoriteTracks);

  const [anchorPoint, setAnchorPoint] = useState({ x: 0, y: 0 });
  const [show, setShow] = useState(false);

  const contextMenuShow = useSelector(selectShowContextMenu);

  const history = useHistory();
  const theme = useTheme();

  const track = props.track;

  useEffect(() => {
    if (!contextMenuShow) {
      setShow(false);
    }
  });

  const dispatch = useDispatch();

  const playerState = useSelector(selectPlayerState);
  const playlists = useSelector(selectPlaylists);

  const tableRowSelectedColor =
    playerState?.track?.trackId === track.track.trackId
      ? rgba(theme.green, 0.15)
      : 'transparent';

  const { actions: favoriteActions } = useFavoriteSlice();
  const { actions: contextMenuActions } = useContextMenuSlice();
  const { actions: playerStateActions } = usePlayerStateSlice();
  const { actions: playlistsActions } = usePlaylistsSlice();

  const removeTrackFromFavorite = (track: FavoriteTrackRecord) => {
    dispatch(favoriteActions.removeTrackFromFavorite(track));
  };

  const contextMenuHandler = (event, track: FavoriteTrackRecord) => {
    event.preventDefault();
    setAnchorPoint({ x: event.pageX, y: event.pageY });
    if (!contextMenuShow) {
      setShow(true);
      dispatch(contextMenuActions.setShow(true));
    }
  };

  const setPlayerTrack = (event, track: Track) => {
    dispatch(playerStateActions.setTrack(track));
  };

  const addTrackToPlaylistHandler = (
    event,
    trackId: string,
    playlistId: string,
  ) => {
    const request: AddTrackToPlaylistRequest = {
      playlistId: playlistId,
      trackId: trackId,
    };
    dispatch(playlistsActions.addTrackToPlaylist(request));
  };

  return (
    <>
      {show && contextMenuShow ? (
        <Menu style={{ top: anchorPoint.y, left: anchorPoint.x }}>
          <MenuItem
            onClick={e =>
              history.push(`/artist/${track.track.artist.artistId}`)
            }
          >
            Перейти к исполнителю
          </MenuItem>
          <MenuItem
            onClick={e => history.push(`/album/${track.track.album.albumId}`)}
          >
            Перейти к альбому
          </MenuItem>
          <MenuItem onClick={e => removeTrackFromFavorite(track)}>
            Удалить из избранного
          </MenuItem>
          {playlists?.map(p => (
            <MenuItem
              onClick={e =>
                addTrackToPlaylistHandler(e, track.track.trackId, p.playlistId)
              }
              key={p.playlistId}
            >
              Добавить в плейлист {p.name}
            </MenuItem>
          ))}
        </Menu>
      ) : null}
      <tr
        style={{ background: tableRowSelectedColor }}
        onClick={e => {
          setShow(false);
          setPlayerTrack(e, track.track);
        }}
        onContextMenu={e => contextMenuHandler(e, track)}
      >
        <td>
          <ActiveHeartIcon onClick={() => removeTrackFromFavorite(track)} />
        </td>
        <td>{track.track.name}</td>
        <td>{track.track.artist.name}</td>
        <td>{track.track.album.name}</td>
        <td>{track.creationDate}</td>
      </tr>
    </>
  );
}

const ActiveHeartIcon = styled(ActiveHeartSvg)`
  width: 24px;
  height: 24px;
`;

const Menu = styled.ul`
  background-color: ${p => p.theme.background};
  border-radius: 7px;
  padding: 5px 0 5px 0;
  width: 250px;
  height: auto;
  margin: 0;
  position: absolute;
  list-style: none;
  box-shadow: 0 0 5px 0 #ccc;
  transition: opacity 0.5s linear;
`;

const MenuItem = styled.li`
  padding-left: 15px;
  padding-right: 15px;
  padding-top: 7px;
  padding-bottom: 7px;
  font-size: 1.15rem;
  font-weight: bolder;
`;
