import { lazyLoad } from 'utils/loadable';

export const FavoriteTracksPage = lazyLoad(
  () => import('./index'),
  module => module.FavoriteTracksPage,
);
