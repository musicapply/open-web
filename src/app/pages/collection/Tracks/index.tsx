import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import styled from 'styled-components/macro';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useFavoriteSlice } from './slice';
import { selectFavoriteTracks } from './slice/selector';
import { media } from 'styles/media';

import { FavoriteTable } from './TrackTable';

const header = ['', 'Название', 'Исполнитель', 'Альбом', 'Дата'];

export function FavoriteTracksPage(props) {
  const favoriteTracks = useSelector(selectFavoriteTracks);

  const dispatch = useDispatch();

  const { actions: favoriteActions } = useFavoriteSlice();

  const useEffectOnMount = (effect: React.EffectCallback) => {
    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(effect, []);
  };

  useEffectOnMount(() => {
    dispatch(favoriteActions.loadFavorite());
  });

  return (
    <>
      <Helmet>
        <title>Избранное</title>
        <meta name="description" content="Избранное" />
      </Helmet>
      <PageWrapper>
        <PageTitle>Избранное</PageTitle>
        <TrackSection>
          <FavoriteTable tracks={favoriteTracks} />
        </TrackSection>
      </PageWrapper>
    </>
  );
}

const PageTitle = styled.h1`
  font-size: 2rem;
  font-weight: bolder;
`;

const PageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding-left: 10px;
  padding-right: 10px;
  width: 100%;

  ${media.small`
    padding-left: 10px;
    padding-right: 10px;
  `}

  ${media.medium`
    padding-left: 54px;
    padding-right: 54px;
  `}

  ${media.large`
    padding-left: 54px;
    padding-right: 54px;
  `}

  ${media.xlarge`
    padding-left: 54px;
    padding-right: 54px;
  `}
`;

const TrackSection = styled.div`
  margin-top: 50px;
  display: flex;
  flex-direction: column;
  width: 100%;
`;
