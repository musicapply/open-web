import { FavoriteTrackRecord } from 'types/FavoriteTrackRecord';

export function saveFavoriteTracksToLocalStorage(
  favorite: FavoriteTrackRecord[],
) {
  window.localStorage &&
    localStorage.setItem('favorite', JSON.stringify(favorite));
}

export function getFavoriteTracksFromLocalStorage():
  | FavoriteTrackRecord[]
  | null {
  return window.localStorage
    ? (JSON.parse(
        <string>localStorage.getItem('favorite'),
      ) as FavoriteTrackRecord[]) || null
    : null;
}
