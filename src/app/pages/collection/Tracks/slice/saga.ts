import {
  call,
  put,
  select,
  takeLatest,
  takeEvery,
  delay,
} from 'redux-saga/effects';
import { favoriteActions as actions } from '.';
import { getAccessTokenFromLocalStorage } from 'utils/tokenUtils';
import { request } from 'utils/request';
import { FavoriteTrackRecord } from 'types/FavoriteTrackRecord';
import { PayloadAction } from '@reduxjs/toolkit';
import { Track } from 'types/Track';

export function* getFavoriteTracks() {
  const requestUrl: string =
    'https://api.musicapply.ru/collection/favorite/all';
  const accessToken: string | null = getAccessTokenFromLocalStorage();

  const favorite: FavoriteTrackRecord[] = yield call(request, requestUrl, {
    headers: {
      Authorization: `Bearer ${accessToken}`,
      'X-Access-Token': `${accessToken}`,
    },
  });

  yield put(actions.favoriteLoaded(favorite));
}

export function* addTrackToFavorite(action: PayloadAction<Track>) {
  const track: Track = yield action.payload;
  const accessToken: string | null = getAccessTokenFromLocalStorage();

  const requestUrl: string = `https://api.musicapply.ru/collection/favorite/add/${track.trackId}`;

  try {
    const response: any = yield call(request, requestUrl, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${accessToken}`,
        'X-Access-Token': `${accessToken}`,
      },
    });
  } catch (err: any) {
    console.error('Error parse JSON');
  }

  yield put(actions.loadFavorite());
}

export function* deleteTrackFromFavorite(
  action: PayloadAction<FavoriteTrackRecord | Track>,
) {
  const trackRecord: FavoriteTrackRecord | Track = yield action.payload;
  const accessToken: string | null = getAccessTokenFromLocalStorage();

  let requestUrl: string = '';

  if ('track' in trackRecord) {
    requestUrl = `https://api.musicapply.ru/collection/favorite/remove/${trackRecord.track.trackId}`;
  } else {
    requestUrl = `https://api.musicapply.ru/collection/favorite/remove/${trackRecord.trackId}`;
  }

  try {
    const response: any = yield call(request, requestUrl, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${accessToken}`,
        'X-Access-Token': `${accessToken}`,
      },
    });
  } catch (err: any) {
    console.error('Error parse JSON');
  }
  yield put(actions.loadFavorite());
}

export function* favoriteSaga() {
  yield takeLatest(actions.loadFavorite.type, getFavoriteTracks);
  yield takeEvery(
    actions.removeTrackFromFavorite.type,
    deleteTrackFromFavorite,
  );
  yield takeEvery(actions.addTrackToFavorite.type, addTrackToFavorite);
}
