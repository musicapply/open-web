import { createSelector } from '@reduxjs/toolkit';
import { RootState } from 'types';
import { initialState } from './index';

const selectDomain = (state: RootState) => state.favorite || initialState;

export const selectFavoriteTracks = createSelector(
  [selectDomain],
  favoriteState => favoriteState?.favorite,
);
