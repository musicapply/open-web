import { FavoriteTracksState } from 'types/FavoriteTracksState';
import { getFavoriteTracksFromLocalStorage } from './utils';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { PayloadAction } from '@reduxjs/toolkit';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { FavoriteTrackRecord } from 'types/FavoriteTrackRecord';
import { favoriteSaga } from './saga';
import { Track } from 'types/Track';

export const initialState: FavoriteTracksState = {
  favorite: getFavoriteTracksFromLocalStorage() || null,
};

export const slice = createSlice({
  name: 'favorite',
  initialState,
  reducers: {
    loadFavorite(state) {},
    favoriteLoaded(state, action: PayloadAction<FavoriteTrackRecord[]>) {
      state.favorite = action.payload;
    },
    removeTrackFromFavorite(
      state,
      action: PayloadAction<FavoriteTrackRecord | Track>,
    ) {},
    addTrackToFavorite(state, action: PayloadAction<Track>) {},
  },
});

export const { actions: favoriteActions, reducer } = slice;

export const useFavoriteSlice = () => {
  useInjectReducer({
    key: slice.name,
    reducer: slice.reducer,
  });
  useInjectSaga({
    key: slice.name,
    saga: favoriteSaga,
  });
  return {
    actions: slice.actions,
  };
};
