import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { useParams } from 'react-router';
import styled from 'styled-components/macro';
import { useEffect, useState } from 'react';
import { getAlbumById } from 'utils/api/Album';
import { Album } from 'types/Album';
import { media } from 'styles/media';
import { ImageViewer } from 'app/components/ImageViewer/ImageViewer';

import { ReactComponent as ActiveHeartSvg } from './assets/ActiveHeartSvg.svg';
import { ReactComponent as InactiveHeartSvg } from './assets/InactiveHeartSvg.svg';
import { AlbumTracksTable } from './TrackTable';
import { useDispatch, useSelector } from 'react-redux';
import { selectLibraryAlbums } from '../collection/Albums/slice/selector';
import { useLibraryAlbumsSlice } from '../collection/Albums/slice';

export function AlbumPage(props) {
  const [album, setAlbum] = useState<Album | null>(null);

  // @ts-ignore
  const { albumId } = useParams();
  const libraryAlbums = useSelector(selectLibraryAlbums);
  const dispatch = useDispatch();
  const { actions: libraryAlbumActions } = useLibraryAlbumsSlice();

  const useEffectOnMount = (effect: React.EffectCallback) => {
    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(effect, []);
  };

  useEffectOnMount(() => {
    // @ts-ignore
    getAlbumById(albumId).then(res => setAlbum(res));
  });

  const removeAlbumFromFavorite = (album: Album) => {
    dispatch(libraryAlbumActions.removeAlbumFromLibrary(album));
  };

  const addAlbumToFavorite = (album: Album) => {
    dispatch(libraryAlbumActions.addAlbumToLibrary(album));
  };

  return (
    <>
      <Helmet>
        <title>{album?.name}</title>
        <meta name="description" content={`${album?.name}`} />
      </Helmet>
      <PageWrapper>
        <PageTitle>{album?.name}</PageTitle>
        <AlbumInfoWrapper>
          <ImageViewer imageUrl={album?.imageUrl} width={200} height={200} />
          <AlbumActionsWrapper>
            {
              // @ts-ignore
              libraryAlbums?.filter(
                libAlbum => libAlbum.album.albumId === album?.albumId,
              ).length > 0 ? (
                <ActiveHeartIcon
                  onClick={e => removeAlbumFromFavorite(album!)}
                />
              ) : (
                <InactiveHeartIcon onClick={e => addAlbumToFavorite(album!)} />
              )
            }
          </AlbumActionsWrapper>
        </AlbumInfoWrapper>
        <TracksWrapper>
          <AlbumTracksTable tracks={album?.tracks} />
        </TracksWrapper>
      </PageWrapper>
    </>
  );
}

const PageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding-left: 10px;
  padding-right: 10px;
  width: 100%;

  ${media.small`
    padding-left: 10px;
    padding-right: 10px;
  `}

  ${media.medium`
    padding-left: 54px;
    padding-right: 54px;
  `}

  ${media.large`
    padding-left: 54px;
    padding-right: 54px;
  `}

  ${media.xlarge`
    padding-left: 54px;
    padding-right: 54px;
  `}
`;

const ActiveHeartIcon = styled(ActiveHeartSvg)`
  width: 24px;
  height: 24px;
`;

const InactiveHeartIcon = styled(InactiveHeartSvg)`
  width: 24px;
  height: 24px;
`;

const PageTitle = styled.h1`
  font-size: 2rem;
  font-weight: bolder;
`;

const AlbumInfoWrapper = styled.div`
  display: flex;
  flex-direction: row;
`;

const TracksWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: 35px;
`;

const AlbumActionsWrapper = styled.div`
  padding-left: 15px;
  display: flex;
  flex-direction: column;
`;
