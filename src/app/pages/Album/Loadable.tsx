import { lazyLoad } from 'utils/loadable';

export const AlbumPage = lazyLoad(
  () => import('./index'),
  module => module.AlbumPage,
);
