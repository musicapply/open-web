import * as React from 'react';
import styled, { useTheme } from 'styled-components/macro';
import { ReactComponent as ActiveHeartSvg } from './assets/ActiveHeartSvg.svg';
import { ReactComponent as InactiveHeartSvg } from './assets/InactiveHeartSvg.svg';
import { FavoriteTrackRecord } from 'types/FavoriteTrackRecord';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useState } from 'react';

import {
  useParams,
  useLocation,
  useHistory,
  useRouteMatch,
} from 'react-router-dom';
import { selectShowContextMenu } from '../../../components/ContextMenu/slice/selector';
import { useFavoriteSlice } from '../../collection/Tracks/slice';
import { useContextMenuSlice } from '../../../components/ContextMenu/slice';
import { Track } from 'types/Track';
import { selectFavoriteTracks } from '../../collection/Tracks/slice/selector';
import { rgba } from 'polished';
import { selectPlayerState } from '../../../components/PlayingBar/slice/selector';
import { usePlayerStateSlice } from '../../../components/PlayingBar/slice';

function formatReleaseDate(releaseDate: string): string {
  return releaseDate.substring(0, releaseDate.length - 9);
}

interface TrackRowProps {
  track: Track;
}

export function TrackRow(props: TrackRowProps) {
  const favoriteTracks = useSelector(selectFavoriteTracks);

  const [anchorPoint, setAnchorPoint] = useState({ x: 0, y: 0 });
  const [show, setShow] = useState(false);

  const contextMenuShow = useSelector(selectShowContextMenu);

  const history = useHistory();
  const theme = useTheme();

  const track = props.track;

  useEffect(() => {
    if (!contextMenuShow) {
      setShow(false);
    }
  });

  const playerState = useSelector(selectPlayerState);

  const tableRowSelectedColor =
    playerState?.track?.trackId === track.trackId
      ? rgba(theme.green, 0.15)
      : 'transparent';

  const dispatch = useDispatch();
  const { actions: favoriteActions } = useFavoriteSlice();
  const { actions: contextMenuActions } = useContextMenuSlice();
  const { actions: playerStateActions } = usePlayerStateSlice();

  const removeTrackFromFavorite = (track: Track) => {
    dispatch(favoriteActions.removeTrackFromFavorite(track));
  };

  const addTrackToFavorite = (track: Track) => {
    dispatch(favoriteActions.addTrackToFavorite(track));
  };

  const contextMenuHandler = event => {
    event.preventDefault();
    setAnchorPoint({ x: event.pageX, y: event.pageY });
    if (!contextMenuShow) {
      setShow(true);
      dispatch(contextMenuActions.setShow(true));
    }
  };

  const setPlayerTrack = (event, track: Track) => {
    dispatch(playerStateActions.setTrack(track));
  };

  return (
    <>
      {show && contextMenuShow ? (
        <Menu style={{ top: anchorPoint.y, left: anchorPoint.x }}>
          <MenuItem>Добавить в плейлист (-)</MenuItem>
          <MenuItem
            onClick={e => history.push(`/artist/${track.artist.artistId}`)}
          >
            Перейти к исполнителю
          </MenuItem>
        </Menu>
      ) : null}
      <tr
        style={{ background: tableRowSelectedColor }}
        onClick={e => {
          setShow(false);
          setPlayerTrack(e, track);
        }}
        onContextMenu={e => contextMenuHandler(e)}
      >
        <td>
          {
            // @ts-ignore
            favoriteTracks?.filter(fav => fav.track.trackId === track.trackId)
              .length > 0 ? (
              <ActiveHeartIcon onClick={e => removeTrackFromFavorite(track)} />
            ) : (
              <InactiveHeartIcon onClick={e => addTrackToFavorite(track)} />
            )
          }
        </td>
        <td>{track.name}</td>
        <td>{track.artist.name}</td>
        <td>{track.album.name}</td>
        <td>{formatReleaseDate(track.album.releaseDate)}</td>
      </tr>
    </>
  );
}

const InactiveHeartIcon = styled(InactiveHeartSvg)`
  width: 24px;
  height: 24px;
`;

const ActiveHeartIcon = styled(ActiveHeartSvg)`
  width: 24px;
  height: 24px;
`;

const Menu = styled.ul`
  background-color: ${p => p.theme.background};
  border-radius: 7px;
  padding: 5px 0 5px 0;
  width: 250px;
  height: auto;
  margin: 0;
  position: absolute;
  list-style: none;
  box-shadow: 0 0 5px 0 #ccc;
  transition: opacity 0.5s linear;
`;

const MenuItem = styled.li`
  padding-left: 15px;
  padding-right: 15px;
  padding-top: 7px;
  padding-bottom: 7px;
  font-size: 1.15rem;
  font-weight: bolder;
`;
