import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import styled from 'styled-components/macro';
import Avatar from '../../components/Avatar/';
import { useDispatch, useSelector } from 'react-redux';
import { selectUser } from './slice/selectors';
import { useUserSlice } from './slice';
import { useEffect, useState } from 'react';

import { ReactComponent as AddButtonSvg } from './assets/AddButtonSvg.svg';
import { PaymentCard } from '../../components/PaymentCard';
import { usePaymentProfileSlice } from './PaymentProfile/slice';
import { selectPaymentProfile } from './PaymentProfile/slice/selector';
import { PaymentMethod } from '../../../types/PaymentMethod';
import { useTransactionsSlice } from './Transactions/slice';
import { selectTransactions } from './Transactions/slice/selectors';

import { media } from '../../../styles/media';
import { useCustomerSlice } from './Customer';
import { selectCustomer } from './Customer/selector';
import Popup from 'reactjs-popup';
import { NewPaymentMethodCard } from '../../components/NewPaymentMethodCard';
import { useKeycloak } from '@react-keycloak/web';

const header = ['Товар', 'Платежный метод', 'Сумма', 'Дата'];

const convertCurrency = (currency: string) => {
  return currency === 'rub' ? 'руб.' : currency === 'usd' ? 'usd.' : 'у.е';
};

const formatPrice = (unitAmount: number, currency: string): string => {
  return `${(unitAmount / 100).toFixed(2)} ${convertCurrency(currency)}`;
};

export function UserPage() {
  const [
    newPaymentMethodCardDialogIsOpen,
    setNewPaymentMethodCardDialogIsOpen,
  ] = useState(false);
  const [disabledCancel, setDisabledCancel] = useState(false);
  const [disabledApply, setDisabledApply] = useState(false);

  const { initialized, keycloak } = useKeycloak();

  const user = useSelector(selectUser);
  const paymentProfile = useSelector(selectPaymentProfile);
  const transactions = useSelector(selectTransactions);
  const customer = useSelector(selectCustomer);

  const { actions } = useUserSlice();
  const { actions: paymentProfileActions } = usePaymentProfileSlice();
  const { actions: transactionActions } = useTransactionsSlice();
  const { actions: customerActions } = useCustomerSlice();
  const dispatch = useDispatch();

  const useEffectOnMount = (effect: React.EffectCallback) => {
    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(effect, []);
  };

  useEffectOnMount(() => {
    dispatch(actions.loadUser());
    dispatch(customerActions.loadCustomer());
    dispatch(paymentProfileActions.loadPaymentProfile());
    dispatch(transactionActions.loadTransactions());
  });

  const setDefaultPaymentMethod = (paymentMethod: PaymentMethod) => {
    dispatch(paymentProfileActions.setDefaultPaymentMethod(paymentMethod));
  };

  const deletePaymentProfile = (paymentMethod: PaymentMethod) => {
    dispatch(paymentProfileActions.deletePaymentMethod(paymentMethod));
  };

  const cancelSubscription = event => {
    dispatch(paymentProfileActions.cancelSubscription());
    setDisabledCancel(true);
    setTimeout(() => {
      dispatch(actions.loadUser());
      dispatch(customerActions.loadCustomer());
      dispatch(paymentProfileActions.loadPaymentProfile());
      dispatch(transactionActions.loadTransactions());
      setDisabledCancel(false);
    }, 3500);
  };

  const applySubscription = event => {
    setDisabledApply(true);
    dispatch(paymentProfileActions.applySubscription());
    setTimeout(() => {
      dispatch(actions.loadUser());
      dispatch(customerActions.loadCustomer());
      dispatch(paymentProfileActions.loadPaymentProfile());
      dispatch(transactionActions.loadTransactions());
      setDisabledApply(false);
    }, 9000);
  };

  return (
    <>
      <Helmet>
        <title>Профиль</title>
        <meta name="description" content="Профиль" />
      </Helmet>
      <Popup
        open={newPaymentMethodCardDialogIsOpen}
        onClose={() => setNewPaymentMethodCardDialogIsOpen(false)}
        position="center center"
        modal
        nested
      >
        {/*// @ts-ignore*/}
        {close => <NewPaymentMethodCard closeCallback={close} />}
      </Popup>
      <PageWrapper>
        <PageTitle>Профиль</PageTitle>
        <UserInfoSection>
          <AvatarWrapper>
            <Avatar
              imageUrl="https://sun9-41.userapi.com/s/v1/ig2/IUKDoPh4IatfaPl2fOIsGjfmSEMHbfw2aAcgNScTn4j_VXdVB70dhAM5AOOjXJai_YSGuyISg7_sz48X3IMGGyVI.jpg?size=1080x1029&quality=96&type=album"
              username=""
              size="xlg"
            />
            <Username>{user?.username!}</Username>
          </AvatarWrapper>
          <CustomerInfo>
            {customer?.status !== null && customer?.status !== undefined ? (
              customer?.status === 'ACTIVE' ? (
                <p>Подписка оформлена</p>
              ) : (
                <p>Подписка не оформлена</p>
              )
            ) : null}
            {customer?.start !== null && customer?.start !== undefined ? (
              <p>Начало: {customer?.start}</p>
            ) : null}
            {customer?.nextPayment !== null &&
            customer?.nextPayment !== undefined ? (
              <p>Следующий платеж: {customer?.nextPayment}</p>
            ) : null}
            {customer?.status !== null && customer?.status !== undefined ? (
              customer?.status === 'ACTIVE' ? (
                <CancelSubscriptionButton
                  disabled={disabledCancel}
                  onClick={e => cancelSubscription(e)}
                >
                  Отменить подписку
                </CancelSubscriptionButton>
              ) : (
                <ApplySubscriptionButton
                  disabled={disabledApply}
                  onClick={e => applySubscription(e)}
                >
                  Оформить подписку (169 руб/мес)
                </ApplySubscriptionButton>
              )
            ) : null}
          </CustomerInfo>
          <LogoutButton onClick={e => keycloak.logout()}>Выйти</LogoutButton>
        </UserInfoSection>
        <PaymentMethodSection>
          <PaymentMethodTitleSection>
            <PaymentMethodsTitle>Платежные методы</PaymentMethodsTitle>
            <AddButton
              onClick={() => setNewPaymentMethodCardDialogIsOpen(true)}
            />
          </PaymentMethodTitleSection>
          <PaymentCardsSection>
            {paymentProfile?.paymentMethods?.map(card => (
              <PaymentCard
                key={card.paymentMethodId}
                card={card}
                setDefaultPaymentMethodCallback={setDefaultPaymentMethod}
                deletePaymentMethodCallback={deletePaymentProfile}
                defaultPaymentMethodId={
                  paymentProfile?.defaultPaymentMethod?.paymentMethodId
                }
              />
            ))}
          </PaymentCardsSection>
        </PaymentMethodSection>
        <TransactionSection>
          <TransactionMethodTitle>Транзакции</TransactionMethodTitle>
          <Table>
            <thead>
              <tr>
                {header.map((h, i) => (
                  <th key={i}>{h}</th>
                ))}
              </tr>
            </thead>
            <tbody>
              {transactions?.map((k, i) => {
                let data = k;
                // @ts-ignore
                return (
                  <tr key={i}>
                    <td>{data.product.name}</td>
                    <td>
                      {
                        // @ts-ignore
                        paymentProfile?.paymentMethods?.find(
                          pm => pm.paymentMethodId === data.paymentMethodId,
                        ) === undefined
                          ? ''
                          : // @ts-ignore
                            paymentProfile?.paymentMethods?.find(
                              pm => pm.paymentMethodId === data.paymentMethodId,
                            ).cardBrand
                      }
                    </td>
                    <td>{formatPrice(data.amountPaid, data.currency)}</td>
                    <td>{data.paymentDate}</td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </TransactionSection>
      </PageWrapper>
    </>
  );
}

const LogoutButton = styled.button`
  border-radius: 10px;
  height: 46px;
  min-height: 46px;
  max-height: 46px;
  background: red;
  border: none;
  font-size: 1.25rem;
  font-weight: bolder;
  color: white;
`;

const CancelSubscriptionButton = styled.button`
  border-radius: 10px;
  height: 46px;
  min-height: 46px;
  max-height: 46px;
  background: red;
  border: none;
  font-size: 1.25rem;
  font-weight: bolder;
  color: white;
`;

const ApplySubscriptionButton = styled.button`
  border-radius: 10px;
  height: 46px;
  min-height: 46px;
  max-height: 46px;
  background: ${p => p.theme.green};
  border: none;
  font-size: 1.25rem;
  font-weight: bolder;
  color: white;
`;

const Table = styled.table`
  td {
    padding-top: 10px;
  }

  thead {
    font-size: 1rem;
    text-transform: uppercase;
  }

  thead tr {
    border-bottom: 0.1em solid ${p => p.theme.table.border};
  }

  thead th {
    font-weight: 400;
    text-align: start;
  }

  tbody td {
    font-size: 1.15rem;
    font-weight: 400;

    svg {
      margin-top: -7px;
    }
  }

  tbody tr {
    border-bottom: 0.01em solid ${p => p.theme.table.border};
  }
`;

const CustomerInfo = styled.div`
  padding-left: 25px;
  display: flex;
  flex-direction: column;

  p {
    margin-top: 2px;
    margin-bottom: 2px;

    font-size: 1rem;
    font-weight: bolder;
  }
`;

const AddButton = styled(AddButtonSvg)`
  width: 24px;
  height: 24px;
`;

const PageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding-left: 10px;
  padding-right: 10px;
  width: 100%;

  ${media.small`
    padding-left: 10px;
    padding-right: 10px;
  `}

  ${media.medium`
    padding-left: 54px;
    padding-right: 54px;
  `}

  ${media.large`
    padding-left: 54px;
    padding-right: 54px;
  `}

  ${media.xlarge`
    padding-left: 54px;
    padding-right: 54px;
  `}
`;

const PageTitle = styled.h1`
  font-size: 2rem;
  font-weight: bolder;
`;

const UserInfoSection = styled.div`
  display: flex;
  flex-direction: row;

  ${LogoutButton} {
    margin-left: auto;
  }
`;

const AvatarWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: auto;
`;

const Username = styled.span`
  font-weight: bolder;
  font-size: 1.25rem;
  text-align: center;
`;

const PaymentMethodSection = styled.div`
  display: flex;
  flex-direction: column;
`;

const TransactionSection = styled.div`
  display: flex;
  flex-direction: column;
`;

const TransactionMethodTitle = styled.h2`
  font-size: 1.5rem;
  font-weight: bolder;
`;

const PaymentMethodTitleSection = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;

  ${AddButton} {
    margin-left: 15px;
  }
`;

const PaymentMethodsTitle = styled.h2`
  font-size: 1.5rem;
  font-weight: bolder;
`;

const PaymentCardsSection = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
  width: 100%;
`;
