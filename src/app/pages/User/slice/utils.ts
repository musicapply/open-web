import { User } from '../../../../types/User';

export function saveUserToStorage(user: User) {
  window.localStorage && localStorage.setItem('user', JSON.stringify(user));
}

export function getUserFromLocalStorage(): User | null {
  return window.localStorage
    ? (JSON.parse(<string>localStorage.getItem('user')) as User) || null
    : null;
}
