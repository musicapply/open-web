import { call, put, select, takeLatest, delay } from 'redux-saga/effects';
import { request } from 'utils/request';
import { userActions as actions } from '.';
import { User } from 'types/User';
import { getAccessTokenFromLocalStorage } from 'utils/tokenUtils';

export function* getUser() {
  const requestUrl: string = 'https://api.musicapply.ru/user/me';

  const accessToken: string | null = getAccessTokenFromLocalStorage();

  const user: User = yield call(request, requestUrl, {
    headers: {
      Authorization: `Bearer ${accessToken}`,
      'X-Access-Token': `${accessToken}`,
    },
  });
  yield put(actions.userLoaded(user));
}

export function* userSaga() {
  yield takeLatest(actions.loadUser.type, getUser);
}
