import { createSlice } from 'utils/@reduxjs/toolkit';
import { PayloadAction } from '@reduxjs/toolkit';
import { UserState } from '../../../../types/UserState';
import { User } from '../../../../types/User';
import { userSaga } from './saga';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { getUserFromLocalStorage } from './utils';

export const initialState: UserState = {
  user: getUserFromLocalStorage() || null,
};

const slice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    loadUser(state) {},
    userLoaded(state, action: PayloadAction<User>) {
      const user = action.payload;
      state.user = user;
    },
  },
});

export const { actions: userActions, reducer } = slice;

export const useUserSlice = () => {
  useInjectReducer({
    key: slice.name,
    reducer: slice.reducer,
  });
  useInjectSaga({
    key: slice.name,
    saga: userSaga,
  });
  return {
    actions: slice.actions,
  };
};
