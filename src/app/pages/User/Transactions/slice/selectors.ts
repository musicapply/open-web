import { createSelector } from '@reduxjs/toolkit';
import { RootState } from 'types';
import { initialState } from './index';

const selectDomain = (state: RootState) => state.transactions || initialState;

export const selectTransactions = createSelector(
  [selectDomain],
  transactionsState => transactionsState?.transactions,
);
