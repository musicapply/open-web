import { call, put, select, takeLatest, delay } from 'redux-saga/effects';
import { request } from 'utils/request';
import { transactionActions as actions } from '.';
import { getAccessTokenFromLocalStorage } from '../../../../../utils/tokenUtils';
import { Transaction } from '../../../../../types/Transaction';

export function* getTransactions() {
  const requestUrl: string = 'https://api.musicapply.ru/transaction/all';

  const accessToken: string | null = getAccessTokenFromLocalStorage();

  const transactions: Transaction[] = yield call(request, requestUrl, {
    headers: {
      Authorization: `Bearer ${accessToken}`,
      'X-Access-Token': `${accessToken}`,
    },
  });

  yield put(actions.transactionsLoaded(transactions));
}

export function* transactionsSaga() {
  yield takeLatest(actions.loadTransactions.type, getTransactions);
}
