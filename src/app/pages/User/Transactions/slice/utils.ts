import { Transaction } from '../../../../../types/Transaction';

export function saveTransactionsToLocalStorage(transactions: Transaction[]) {
  window.localStorage &&
    localStorage.setItem('transactions', JSON.stringify(transactions));
}

export function getTransactionsFromLocalStorage(): Transaction[] | null {
  return window.localStorage
    ? (JSON.parse(
        <string>localStorage.getItem('transactions'),
      ) as Transaction[]) || null
    : null;
}
