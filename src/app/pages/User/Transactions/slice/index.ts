import { TransactionsState } from '../../../../../types/TransactionsState';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { getTransactionsFromLocalStorage } from './utils';
import { PayloadAction } from '@reduxjs/toolkit';
import { Transaction } from '../../../../../types/Transaction';
import {
  useInjectReducer,
  useInjectSaga,
} from '../../../../../utils/redux-injectors';
import { transactionsSaga } from './saga';

export const initialState: TransactionsState = {
  transactions: getTransactionsFromLocalStorage() || null,
};

export const slice = createSlice({
  name: 'transactions',
  initialState,
  reducers: {
    loadTransactions(state) {},
    transactionsLoaded(state, action: PayloadAction<Transaction[]>) {
      const transactions: Transaction[] = action.payload;
      state.transactions = transactions;
    },
  },
});

export const { actions: transactionActions, reducer } = slice;

export const useTransactionsSlice = () => {
  useInjectReducer({
    key: slice.name,
    reducer: slice.reducer,
  });
  useInjectSaga({
    key: slice.name,
    saga: transactionsSaga,
  });
  return {
    actions: slice.actions,
  };
};
