import { createSelector } from '@reduxjs/toolkit';
import { RootState } from 'types';
import { initialState } from './index';

const selectDomain = (state: RootState) => state.paymentProfile || initialState;

export const selectPaymentProfile = createSelector(
  [selectDomain],
  paymentProfileState => paymentProfileState?.paymentProfile,
);
