import { PaymentProfile } from '../../../../../types/PaymentProfile';

export function savePaymentProfileToLocalStorage(
  paymentProfile: PaymentProfile,
) {
  window.localStorage &&
    localStorage.setItem('paymentProfile', JSON.stringify(paymentProfile));
}

export function getPaymentProfileFromLocalStorage(): PaymentProfile | null {
  return window.localStorage
    ? (JSON.parse(
        <string>localStorage.getItem('paymentProfile'),
      ) as PaymentProfile) || null
    : null;
}
