import {
  call,
  put,
  select,
  takeLatest,
  takeEvery,
  delay,
} from 'redux-saga/effects';
import { getAccessTokenFromLocalStorage } from 'utils/tokenUtils';
import { PaymentProfile } from 'types/PaymentProfile';
import { request } from 'utils/request';
import { paymentProfileActions as actions } from '.';
import { selectPaymentProfile } from './selector';
import { PayloadAction } from '@reduxjs/toolkit';
import { PaymentMethod } from 'types/PaymentMethod';
import { AddPaymentMethodRequest } from '../../../../../types/AddPaymentMethodRequest';

export function* getPaymentProfile() {
  const requestUrl: string = 'https://api.musicapply.ru/payment_profile/me';

  const accessToken: string | null = getAccessTokenFromLocalStorage();

  const paymentProfile: PaymentProfile = yield call(request, requestUrl, {
    headers: {
      Authorization: `Bearer ${accessToken}`,
      'X-Access-Token': `${accessToken}`,
    },
  });
  yield put(actions.paymentProfileLoaded(paymentProfile));
}

export function* setDefaultPaymentMethod() {
  const paymentProfile: PaymentProfile = yield select(selectPaymentProfile);
  const accessToken: string | null = getAccessTokenFromLocalStorage();
  const requestUrl: string = `https://api.musicapply.ru/payment_profile/payment_method/default/${paymentProfile.defaultPaymentMethod?.paymentMethodId}`;

  try {
    yield call(request, requestUrl, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${accessToken}`,
        'X-Access-Token': `${accessToken}`,
      },
    });
  } catch (err: any) {
    console.error('Error parse JSON');
  }

  yield put(actions.loadPaymentProfile());
}

export function* deletePaymentMethod(action: PayloadAction<PaymentProfile>) {
  const paymentMethod: PaymentMethod = yield action.payload;
  const accessToken: string | null = getAccessTokenFromLocalStorage();
  const requestUrl: string = `https://api.musicapply.ru/payment_profile/payment_method/${paymentMethod.paymentMethodId}`;

  try {
    const response: any = yield call(request, requestUrl, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${accessToken}`,
        'X-Access-Token': `${accessToken}`,
      },
    });
  } catch (err: any) {
    console.error('Error parse JSON');
  }
  yield put(actions.loadPaymentProfile());
}

export function* addPaymentMethod(
  action: PayloadAction<AddPaymentMethodRequest>,
) {
  const requestUrl: string =
    'https://api.musicapply.ru/payment_profile/payment_method/new';
  const accessToken: string | null = getAccessTokenFromLocalStorage();

  try {
    const response: any = yield call(request, requestUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${accessToken}`,
        'X-Access-Token': `${accessToken}`,
      },
      body: JSON.stringify(action.payload),
    });
  } catch (err: any) {
    console.error('Error parse JSON');
  }

  yield put(actions.loadPaymentProfile());
}

export function* cancelSubscription() {
  const requestUrl: string =
    'https://api.musicapply.ru/payment_profile/subscription/cancel';
  const accessToken: string | null = getAccessTokenFromLocalStorage();

  try {
    const response: any = yield call(request, requestUrl, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${accessToken}`,
        'X-Access-Token': `${accessToken}`,
      },
    });
  } catch (err: any) {
    console.error('Error parse JSON');
  }

  yield put(actions.loadPaymentProfile());
}

export function* applySubscription() {
  const requestUrl: string =
    'https://api.musicapply.ru/payment_profile/subscription/new/794d4ea9-3528-4b50-9f7b-bb8b12add915';
  const accessToken: string | null = getAccessTokenFromLocalStorage();

  try {
    const response: any = yield call(request, requestUrl, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${accessToken}`,
        'X-Access-Token': `${accessToken}`,
      },
    });
  } catch (err: any) {
    console.error('Error parse JSON');
  }

  yield put(actions.loadPaymentProfile());
}

export function* paymentProfileSaga() {
  yield takeLatest(actions.loadPaymentProfile.type, getPaymentProfile);
  yield takeLatest(
    actions.setDefaultPaymentMethod.type,
    setDefaultPaymentMethod,
  );
  yield takeEvery(actions.deletePaymentMethod.type, deletePaymentMethod);
  yield takeEvery(actions.addPaymentMethod.type, addPaymentMethod);
  yield takeEvery(actions.cancelSubscription.type, cancelSubscription);
  yield takeEvery(actions.applySubscription.type, applySubscription);
}
