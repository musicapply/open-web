import { PaymentProfileState } from 'types/PaymentProfileState';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { getPaymentProfileFromLocalStorage } from './utils';
import { PayloadAction } from '@reduxjs/toolkit';
import { PaymentProfile } from 'types/PaymentProfile';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { paymentProfileSaga } from './saga';
import { PaymentMethod } from 'types/PaymentMethod';
import { AddPaymentMethodRequest } from '../../../../../types/AddPaymentMethodRequest';

export const initialState: PaymentProfileState = {
  paymentProfile: getPaymentProfileFromLocalStorage() || null,
};

export const slice = createSlice({
  name: 'paymentProfile',
  initialState,
  reducers: {
    loadPaymentProfile(state) {},
    setDefaultPaymentMethod(state, action: PayloadAction<PaymentMethod>) {
      const paymentMethod = action.payload;
      state.paymentProfile!.defaultPaymentMethod = paymentMethod;
    },
    deletePaymentMethod(state, action: PayloadAction<PaymentMethod>) {},
    paymentProfileLoaded(state, action: PayloadAction<PaymentProfile>) {
      const paymentProfile = action.payload;
      state.paymentProfile = paymentProfile;
    },
    addPaymentMethod(state, action: PayloadAction<AddPaymentMethodRequest>) {},
    cancelSubscription(state) {},
    applySubscription(state) {},
  },
});

export const { actions: paymentProfileActions, reducer } = slice;

export const usePaymentProfileSlice = () => {
  useInjectReducer({
    key: slice.name,
    reducer: slice.reducer,
  });
  useInjectSaga({
    key: slice.name,
    saga: paymentProfileSaga,
  });
  return {
    actions: slice.actions,
  };
};
