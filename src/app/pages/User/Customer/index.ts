import { CustomerState } from 'types/CustomerState';
import { getCustomerFromLocalStorage } from './utils';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { PayloadAction } from '@reduxjs/toolkit';
import { Customer } from 'types/Customer';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { customerSaga } from './saga';

export const initialState: CustomerState = {
  customer: getCustomerFromLocalStorage() || null,
};

const slice = createSlice({
  name: 'customer',
  initialState,
  reducers: {
    loadCustomer(state) {},
    customerLoaded(state, action: PayloadAction<Customer>) {
      state.customer = action.payload;
    },
  },
});

export const { actions: customerActions, reducer } = slice;

export const useCustomerSlice = () => {
  useInjectReducer({
    key: slice.name,
    reducer: slice.reducer,
  });
  useInjectSaga({
    key: slice.name,
    saga: customerSaga,
  });
  return {
    actions: slice.actions,
  };
};
