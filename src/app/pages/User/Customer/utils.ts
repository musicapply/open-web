import { Customer } from '../../../../types/Customer';

export function saveCustomerToStorage(customer: Customer) {
  window.localStorage &&
    localStorage.setItem('customer', JSON.stringify(customer));
}

export function getCustomerFromLocalStorage(): Customer | null {
  return window.localStorage
    ? (JSON.parse(<string>localStorage.getItem('customer')) as Customer) || null
    : null;
}
