import { call, put, select, takeLatest, delay } from 'redux-saga/effects';
import { request } from 'utils/request';
import { getAccessTokenFromLocalStorage } from 'utils/tokenUtils';
import { Customer } from '../../../../types/Customer';
import { customerActions as actions } from '.';

export function* getCustomer() {
  const requestUrl: string = 'https://api.musicapply.ru/customer/me';

  const accessToken: string | null = getAccessTokenFromLocalStorage();

  const customer: Customer = yield call(request, requestUrl, {
    headers: {
      Authorization: `Bearer ${accessToken}`,
      'X-Access-Token': `${accessToken}`,
    },
  });
  yield put(actions.customerLoaded(customer));
}

export function* customerSaga() {
  yield takeLatest(actions.loadCustomer.type, getCustomer);
}
