import { createSelector } from '@reduxjs/toolkit';
import { RootState } from 'types';
import { initialState } from './index';

const selectDomain = (state: RootState) => state.customer || initialState;

export const selectCustomer = createSelector(
  [selectDomain],
  customerState => customerState?.customer,
);
