import * as React from 'react';
import styled from 'styled-components/macro';
import { TrackRow } from './TrackRow';
import { Track } from 'types/Track';
import { PlaylistTrackRecord } from 'types/PlaylistTrackRecord';

const header = ['', 'Название', 'Исполнитель', 'Альбом', 'Дата'];

interface PlaylistTracksTableProps {
  tracks: PlaylistTrackRecord[] | undefined;
  playlist: any;
  loadPlaylistCallback: any;
}

export function PlaylistTracksTable(props: PlaylistTracksTableProps) {
  const tracks = props.tracks;

  return (
    <Table onClick={() => console.log(tracks)}>
      <thead>
        <tr>
          {header.map((h, i) => (
            <th key={i}>{h}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {tracks?.map((k, i) => {
          let data = k;
          return (
            <TrackRow
              key={k.id}
              track={k}
              playlist={props.playlist}
              loadPlaylistCallback={props.loadPlaylistCallback}
            />
          );
        })}
      </tbody>
    </Table>
  );
}

const Table = styled.table`
  td {
    padding-top: 10px;
  }

  thead {
    font-size: 1rem;
    text-transform: uppercase;
  }

  thead tr {
    border-bottom: 0.1em solid ${p => p.theme.table.border};
  }

  thead th {
    font-weight: 400;
    text-align: start;
  }

  tbody td {
    font-size: 1.15rem;
    font-weight: 400;

    svg {
      margin-top: -7px;
    }
  }

  tbody tr {
    border-bottom: 0.01em solid ${p => p.theme.table.border};
  }
`;
