import styled from 'styled-components/macro';
import { media } from 'styles/media';
import { Helmet } from 'react-helmet-async';
import * as React from 'react';
import { useParams } from 'react-router';
import { useDispatch } from 'react-redux';
import { useEffect, useState } from 'react';
import { getPlaylistById } from 'utils/api/Playlist';
import { Playlist } from 'types/Playlist';
import { PlaylistTracksTable } from './TrackTable';

import { ReactComponent as RedCancelSvg } from './assets/RedCancelSvg.svg';
import { usePlaylistsSlice } from '../collection/Playlists/slice';
import { useHistory } from 'react-router-dom';

export function PlaylistPage(props) {
  const [playlist, setPlaylist] = useState<Playlist | null>(null);

  // @ts-ignore
  const { playlistId } = useParams();
  const dispatch = useDispatch();
  const history = useHistory();
  const { actions: playlistsActions } = usePlaylistsSlice();

  const useEffectOnMount = (effect: React.EffectCallback) => {
    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(effect, []);
  };

  useEffect(() => {
    // @ts-ignore
    loadPlaylist(playlistId);
  }, [playlistId]);

  const loadPlaylist = playlistId => {
    // @ts-ignore
    getPlaylistById(playlistId).then(res => setPlaylist(res));
  };

  const deletePlaylist = () => {
    dispatch(playlistsActions.removePlaylist(playlistId));
    history.push('/');
  };

  return (
    <>
      <Helmet>
        <title>{playlist?.name}</title>
        <meta name="description" content={`${playlist?.name}`} />
      </Helmet>
      <PageWrapper>
        <TitleWrapper>
          <PageTitle>{playlist?.name}</PageTitle>
          <DeleteButtonIcon onClick={deletePlaylist} />
        </TitleWrapper>
        <TracksWrapper>
          <PlaylistTracksTable
            playlist={playlist}
            tracks={playlist?.tracks}
            loadPlaylistCallback={loadPlaylist}
          />
        </TracksWrapper>
      </PageWrapper>
    </>
  );
}

const DeleteButtonIcon = styled(RedCancelSvg)`
  width: 24px;
  height: 24px;
`;

const TracksWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: 35px;
`;

const TitleWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;

  ${DeleteButtonIcon} {
    margin-left: 15px;
  }
`;

const PageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding-left: 10px;
  padding-right: 10px;
  width: 100%;

  ${media.small`
    padding-left: 10px;
    padding-right: 10px;
  `}

  ${media.medium`
    padding-left: 54px;
    padding-right: 54px;
  `}

  ${media.large`
    padding-left: 54px;
    padding-right: 54px;
  `}

  ${media.xlarge`
    padding-left: 54px;
    padding-right: 54px;
  `}
`;

const PageTitle = styled.h1`
  font-size: 2rem;
  font-weight: bolder;
`;
