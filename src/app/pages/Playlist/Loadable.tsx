import { lazyLoad } from 'utils/loadable';

export const PlaylistPage = lazyLoad(
  () => import('./index'),
  module => module.PlaylistPage,
);
