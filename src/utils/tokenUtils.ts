export function saveAccessTokenToStorage(token: string) {
  window.localStorage && localStorage.setItem('access_token', token);
}

export function getAccessTokenFromLocalStorage(): string | null {
  return window.localStorage
    ? localStorage.getItem('access_token') || null
    : null;
}

export function saveRefreshTokenToStorage(token: string) {
  window.localStorage && localStorage.setItem('refresh_token', token);
}

export function getRefreshTokenFromLocalStorage(): string | null {
  return window.localStorage
    ? localStorage.getItem('refresh_token') || null
    : null;
}
