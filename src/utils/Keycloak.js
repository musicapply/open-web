import Keycloak from 'keycloak-js';

const keycloakConfig = {
  url: 'https://accounts.musicapply.ru/auth/',
  realm: 'musicapply',
  clientId: 'open-web',
};

const keycloak = new Keycloak(keycloakConfig);
export default keycloak;
