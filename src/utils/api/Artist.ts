import { getAccessTokenFromLocalStorage } from '../tokenUtils';
import { request } from '../request';

export function getArtistById(artistId: string) {
  const requestUrl: string = `https://api.musicapply.ru/artist/${artistId}`;
  const accessToken: string | null = getAccessTokenFromLocalStorage();

  return request(requestUrl, {
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });
}

export function searchArtistsLikeName(artistName: string) {
  const requestUrl: string = `https://api.musicapply.ru/artist/search/likeName/${artistName}`;
  const accessToken: string | null = getAccessTokenFromLocalStorage();

  return request(requestUrl, {
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });
}
