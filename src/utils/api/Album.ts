import { getAccessTokenFromLocalStorage } from '../tokenUtils';
import { request } from '../request';

export function getAlbumById(albumId: string) {
  const requestUrl: string = `https://api.musicapply.ru/album/${albumId}`;
  const accessToken: string | null = getAccessTokenFromLocalStorage();

  return request(requestUrl, {
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });
}

export function searchAlbumsLikeName(albumName: string) {
  const requestUrl: string = `https://api.musicapply.ru/album/search/likeName/${albumName}`;
  const accessToken: string | null = getAccessTokenFromLocalStorage();

  return request(requestUrl, {
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });
}
