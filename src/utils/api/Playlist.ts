import { getAccessTokenFromLocalStorage } from '../tokenUtils';
import { request } from '../request';

export function getPlaylistById(playlistId: string) {
  const requestUrl: string = `https://api.musicapply.ru/collection/playlists/getById/${playlistId}`;
  const accessToken: string | null = getAccessTokenFromLocalStorage();

  return request(requestUrl, {
    headers: {
      Authorization: `Bearer ${accessToken}`,
      'X-Access-Token': `${accessToken}`,
    },
  });
}
