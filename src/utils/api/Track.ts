import { getAccessTokenFromLocalStorage } from '../tokenUtils';
import { request } from '../request';

export function searchTracksLikeName(trackName: string) {
  const requestUrl: string = `https://api.musicapply.ru/track/search/likeName/${trackName}`;
  const accessToken: string | null = getAccessTokenFromLocalStorage();

  return request(requestUrl, {
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });
}
