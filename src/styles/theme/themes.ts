const lightTheme = {
  type: 'light',
  primary: 'rgba(241,247,241,1)',
  secondary: 'rgba(241,241,241,1)',
  background: 'rgba(255,255,255,1)',
  green: 'rgba(29,185,84,1)',
  gradient: 'linear-gradient(#39598A, #79D7ED)',
  text: {
    primary: 'rgba(25,20,20,1)',
  },
  scroll: {
    track: 'rgba(196,196,196,1)',
    thumb: 'rgba(25,20,20,1)',
  },
  table: {
    border: 'rgba(100,100,100,1)',
  },
  btn: 'rgba(241,241,241,1)',
  toggleBorder: '#FFF',
};

const darkTheme: Theme = {
  type: 'dark',
  primary: 'rgba(24,24,24,1)',
  secondary: 'rgba(25,20,20,1)',
  background: 'rgba(18,18,18,1)',
  green: 'rgba(29,185,84,1)',
  gradient: 'linear-gradient(#091236, #1E215D)',
  text: {
    primary: 'rgba(241,241,241,1)',
  },
  scroll: {
    track: 'rgba(18,18,18,1)',
    thumb: 'rgba(241,241,241,1)',
  },
  table: {
    border: 'rgba(100,100,100,1)',
  },
  btn: 'rgba(51,51,51,1)',
  toggleBorder: '#6B8096',
};

export type Theme = typeof lightTheme;

export const themes = {
  light: lightTheme,
  dark: darkTheme,
};
