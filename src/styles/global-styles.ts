import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  html,
  body {
    height: 100%;
    width: 100%;
  }

  body {
    font-family: 'Rubik', sans-serif;
    font-weight: lighter;
    color: ${p => p.theme.text.primary};
    font-size: 17px;
    
  }

  ::-webkit-scrollbar {
    width: 7px;
  }

  ::-webkit-scrollbar-thumb {
    background-color: ${p => p.theme.scroll.thumb};
    border-radius: 16px;
  }

  ::-webkit-scrollbar-track {
    background-color: ${p => p.theme.scroll.track};
    border-radius: 16px;
  }

  #root {
    min-height: 100%;
    min-width: 100%;
  }

  p,
  label {
    line-height: 1.5em;
  }

  input, select {
    font-family: inherit;
    font-size: inherit;
  }
`;
